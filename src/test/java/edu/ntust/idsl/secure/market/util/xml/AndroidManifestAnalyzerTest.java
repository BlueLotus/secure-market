package edu.ntust.idsl.secure.market.util.xml;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

/**
 * 
 * @author Carl Lu
 *
 */
public class AndroidManifestAnalyzerTest {
	
	private static AndroidManifestReader androidManifestAnalyzer;
	
	@BeforeClass
	public static void beforeClass() {
		androidManifestAnalyzer = new AndroidManifestReader();
		androidManifestAnalyzer.loadManifest("src/test/resources/TestAndroidManifest.xml");
	}
	
	@Test
	@Ignore
	public void testForObtainVersioncode() {
		assertEquals("1060203", androidManifestAnalyzer.extractVersionCode());
	}

	@Test
	@Ignore
	public void testForObtainPermissions() {
		List<String> permissions = androidManifestAnalyzer.extractAllPermissions();
		assertEquals(28, permissions.size());
		assertEquals("com.evernote", androidManifestAnalyzer.getPackageName());
	}

}
