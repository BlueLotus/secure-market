package edu.ntust.idsl.secure.market.util.xml;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;
import org.xmlpull.v1.XmlPullParser;

/**
 * 
 * @author Carl Lu
 *
 */
public class XMLReaderTest {

	private static XMLReader xmlReader;
	
	@BeforeClass
	public static void beforeClass() {
		xmlReader = new XMLReader();
	}
	
	@Test
	public void testForSetXML() {
		try {
			xmlReader.setXML("src/test/resources/TestAndroidManifest.xml");
		} catch (IOException exception) {
			exception.printStackTrace();
		}
	}
	
	@Test
	public void testForReadXml() {
		try {
			xmlReader.setXML("src/test/resources/TestAndroidManifest.xml");
			int type = xmlReader.getEventType();
			StringBuilder stringBuilder = new StringBuilder();
			while(type != XmlPullParser.END_DOCUMENT) {
				switch (type) {
				case XmlPullParser.START_DOCUMENT:
					stringBuilder.append("Strat Document");
					break;
				case XmlPullParser.START_TAG:
					stringBuilder.append("Start tag: " + xmlReader.getName() + "\n");
					for(int i = 0; i < xmlReader.getAttributeCount(); i++) {
						stringBuilder.append("\t Attr: " + xmlReader.getAttributeName(i) + "--");
						//stringBuilder.append("Value: " + xmlReader.getAttributeValueWithAttrName(xmlReader.getAttributeName(i)));
						stringBuilder.append("Value: " + xmlReader.getAttributeValueWithAttrIndex(i));
					}
					break;
				case XmlPullParser.END_TAG:
					stringBuilder.append("End tag: " + xmlReader.getName());
					break;
				case XmlPullParser.TEXT:
					if(xmlReader.getText().endsWith("")) {
						stringBuilder.append("\t Start text null");
					} else {
						stringBuilder.append("\t Start text: " + xmlReader.getText());
					}
					break;
				default:
					break;
				}
				System.out.println(stringBuilder.toString());
				stringBuilder.delete(0, stringBuilder.length());
				xmlReader.getNext();
				type = xmlReader.getEventType();
			}
		} catch (IOException exception) {
			exception.printStackTrace();
		}
	}
	
	@Test
	public void testForExtractPermissions() {
		try {
			xmlReader.setXML("src/test/resources/TestAndroidManifest.xml");
			int type = xmlReader.getEventType();
			while(type != XmlPullParser.END_DOCUMENT) {
				if(type == XmlPullParser.START_TAG) {
					if(xmlReader.getName().contentEquals("uses-permission")) {
						for(int i = 0; i < xmlReader.getAttributeCount(); i++) {
							String permission = xmlReader.getAttributeValueWithAttrIndex(i);
							System.out.println(permission);
						}
					}
				}
				xmlReader.getNext();
				type = xmlReader.getEventType();
			}
		} catch (IOException exception) {
			exception.printStackTrace();
		}
	}

}
