package edu.ntust.idsl.secure.market.domain;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import edu.ntust.idsl.secure.market.domain.enums.MismatchedResource;
import edu.ntust.idsl.secure.market.domain.enums.VerificationDetail;

/**
 * 
 * @author Carl Lu
 *
 */
public class AppComparedResultTest {
	
	private static AppComparedResult normalComparedResult;
	private static AppComparedResult abnormalComparedResult;
	private static String expectedMismatchedResourcesList;
	
	@BeforeClass
	public static void testInit() {
		normalComparedResult = new AppComparedResult();
		abnormalComparedResult = new AppComparedResult();
		
		expectedMismatchedResourcesList = MismatchedResource.CERTIFICATE.getResourceName().concat(", ").
				concat(MismatchedResource.MNFT_ENTRY_SIZE.getResourceName()).concat(", ").
				concat(MismatchedResource.ANDROID_MNFT_XML.getResourceName()).concat(", ").
				concat(MismatchedResource.CLASSES_DEX.getResourceName());
		
		normalComparedResult.setPassed(true);
		normalComparedResult.setUploadable(true);
		normalComparedResult.setVerificationDetail(VerificationDetail.APP_FROM_GOOGLE_PLAY);
		
		List<MismatchedResource> mismatchedResources = new ArrayList<MismatchedResource>();
		mismatchedResources.add(MismatchedResource.CERTIFICATE);
		mismatchedResources.add(MismatchedResource.MNFT_ENTRY_SIZE);
		mismatchedResources.add(MismatchedResource.ANDROID_MNFT_XML);
		mismatchedResources.add(MismatchedResource.CLASSES_DEX);
		abnormalComparedResult.setMismatchedResources(mismatchedResources);
		abnormalComparedResult.setPassed(false);
		abnormalComparedResult.setUploadable(false);
		abnormalComparedResult.setVerificationDetail(VerificationDetail.REPACKAGED_APP_WITH_MALICIOUS_CODE);
	}

	@Test
	public void testForNormalComparedResult() {
		assertTrue(normalComparedResult.isPassed());
		assertTrue(normalComparedResult.isUploadable());
		assertEquals(VerificationDetail.APP_FROM_GOOGLE_PLAY, normalComparedResult.getVerificationDetail());
	}
	
	@Test
	public void testForAbnormalComparedResult() {
		assertFalse(abnormalComparedResult.isPassed());
		assertFalse(abnormalComparedResult.isUploadable());
		assertTrue(abnormalComparedResult.getMismatchedResources().size() > 0);
		assertEquals(expectedMismatchedResourcesList, abnormalComparedResult.showMismatchedResources());
	}

}
