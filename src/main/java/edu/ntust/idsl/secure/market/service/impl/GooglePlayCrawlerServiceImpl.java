package edu.ntust.idsl.secure.market.service.impl;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.ntust.idsl.secure.market.domain.APKInfo;
import edu.ntust.idsl.secure.market.domain.AppDownloadInfo;
import edu.ntust.idsl.secure.market.domain.enums.AppDownloadStrategy;
import edu.ntust.idsl.secure.market.domain.enums.VerificationDetail;
import edu.ntust.idsl.secure.market.persistence.APKInfoRepository;
import edu.ntust.idsl.secure.market.service.GooglePlayCrawlerService;
import edu.ntust.idsl.secure.market.util.APKFileHandler;
import edu.ntust.idsl.secure.market.util.FileExplorer;
import edu.ntust.idsl.secure.market.util.constant.ResourceConstant;
import edu.ntust.idsl.secure.market.util.constant.WebConstant;
import edu.ntust.idsl.secure.market.util.crawler.GooglePlayCrawler;

/**
 * 
 * @author Carl Lu
 *
 */
@Service
public class GooglePlayCrawlerServiceImpl implements GooglePlayCrawlerService {
	
	private static final Logger logger = LoggerFactory.getLogger(GooglePlayCrawlerServiceImpl.class);
	
	private static final long MAX_REST_TIME = 30 * 1000;
	
	private static final int MAX_SKIP_DOWNLOAD_COUNT = 30;
	
	private static int skipDownloadCount = 0;
	
	@Autowired
	private WebConstant webConstant;
	
	@Autowired
	private GooglePlayCrawler crawler;
	
	@Autowired
	private APKFileHandler apkFileHandler;
	
	@Autowired
	private APKInfoRepository apkInfoRepository;

	@Override
	public InputStream crawlAPKWithPackageName(String packageName) throws Exception {
		crawler.launch();
		return crawler.downloadAPKWithPackageName(packageName);
	}

	@Override
	public void crawlAPKForCategories(List<String> categories) throws Exception {
		crawler.launch();
		categories.forEach(category -> crawlAPKForCategory(category));
	}

	private void crawlAPKForCategory(String category) {
		List<String> packageNames = extractPackageNameListFromCategory(category);
		try {
			packageNames.forEach(packageName -> downloadAPKFile(packageName));
			archiveFinishedCategory(category);
		} catch (Exception e) {
			logger.error("Carwl apk for category {} failed, msg: {}", category, e.getMessage());
			throw new RuntimeException(e);
		}
	}
	
	private List<String> extractPackageNameListFromCategory(String category) {
		List<String> packageNames = new ArrayList<String>();
		try (Stream<String> lines = Files.lines(new File(webConstant.getPathToCrawlerList() + File.separator + category).toPath(), StandardCharsets.UTF_8)) {
			lines.filter(packageName -> !packageName.isEmpty()).
			      forEach(packageName -> packageNames.add(packageName));
			lines.close();
			logger.info("Category {} has {} apps.", category, packageNames.size());
		} catch (IOException e) {
			logger.error("Error when get list from category {}...", category);
		}
		return packageNames;
	}
	
	private boolean downloadAPKFile(String packageName) {
		boolean result = false;
		String apkName = packageName + ResourceConstant.SUFFIX_FOR_APK_FILE;
		try {
			logger.info("Start to download apk: {}", packageName);
			AppDownloadInfo appDownloadInfo = crawler.getAppDownloadInfo(packageName);
			if(appDownloadInfo != null) {
				AppDownloadStrategy downloadStrategy = checkDownloadStrategy(appDownloadInfo);
				checkToSlowDownOrNot(downloadStrategy);
				if(shouldBeDownloaded(downloadStrategy)) {
					crawler.downloadAPKFileWithAppDownloadInfo(appDownloadInfo);
					APKInfo apkInfo = apkFileHandler.processAPKInfo(new APKInfo(appDownloadInfo.getAppName(), apkName));
					persistAPKInfo(apkInfo);
					apkFileHandler.clearTempResources(apkName);
					deleteDownloadedAPK(packageName);
					logger.info("{} has downloaded successfully.", packageName);
					result = true;
				}
			}
		} catch (Exception e) {
			logger.error("Something wrong when downloading apk: {}", packageName);
			if(e.getMessage().contains(ResourceConstant.ERROR_MSG_FOR_APK_ITEM_NOT_FOUND) 
					|| (e.getMessage().contains(ResourceConstant.ERROR_MSG_FOR_ITEM_NOT_FOUND))) {
				logger.error("Could not found this app on google play, skip it.");
			} else if(recursivelyTraceCauseForSpecificExceptionMessage(e, ResourceConstant.ERROR_MSG_FOR_UTF8MB4_NOT_SUPPORTED)) {
				logger.error("Not supported for UTF8MB4 encoding, skip it.");
			} else {
				logger.error("The error msg is: {}", e.getMessage());
			}
			try {
				apkFileHandler.clearTempResources(apkName);
				deleteDownloadedAPK(packageName);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			apkFileHandler.recordDownloadFailedAPK(packageName);
		}
		return result;
	}
	
	private AppDownloadStrategy checkDownloadStrategy(AppDownloadInfo downloadInfo) {
		try {
			AppDownloadStrategy downloadStrategy = AppDownloadStrategy.SKIP_DOWNLOAD;
			List<APKInfo> queryResult = apkInfoRepository.findByApkNameAndVerificationDetailOrderByUploadTimeDesc(
					downloadInfo.getPackageName() + ResourceConstant.SUFFIX_FOR_APK_FILE, 
					VerificationDetail.APP_FROM_GOOGLE_PLAY.getDetailCode());
			if (!queryResult.isEmpty()) {
				if (hasNewVersion(String.valueOf(downloadInfo.getVersionCode()), queryResult.get(0).getVersionCode())) {
					logger.info("New version released, start downloading...");
					downloadStrategy = AppDownloadStrategy.UPDATE_EXISTENT_APP;
				} else {
					logger.info("No new version released, skip download process.");
				}
			} else {
				logger.info("It's a new app, start downloading...");
				downloadStrategy = AppDownloadStrategy.DOWNLOAD_NEW_APP;
			}
			return downloadStrategy;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	private boolean hasNewVersion(String unknownVersion, String knownVersion) {
		return unknownVersion.equalsIgnoreCase(knownVersion) ? false : true;
	}
	
	private boolean shouldBeDownloaded(AppDownloadStrategy downloadStrategy) {
		return downloadStrategy.getStrategyCode() > 1 ? true : false;
	}
	
	private void persistAPKInfo(APKInfo apkInfo) {
		apkInfo.setVerificationDetail(VerificationDetail.APP_FROM_GOOGLE_PLAY.getDetailCode());
		apkInfo.setUploadTime(LocalDateTime.now());
		apkInfoRepository.save(apkInfo);
	}
	
	private void deleteDownloadedAPK(String packageName) {
		File deletable = new File(webConstant.getPathToUploaded() + File.separator + packageName + ResourceConstant.SUFFIX_FOR_APK_FILE);
		deletable.delete();
	}
	
	private void archiveFinishedCategory(String category) throws IOException {
		File origin = null;
		File archived = null;
		try {
			origin = new File(webConstant.getPathToCrawlerList() + File.separator + category);
			archived = new File(webConstant.getPathToArchivedList() + File.separator + category);
			FileExplorer.copyFile(origin, archived, false);
		} catch (IOException e) {
			logger.error("Something wrong happened when archiving category: {}", category);
			throw e;
		}
	}
	
	private void checkToSlowDownOrNot(AppDownloadStrategy appDownloadStrategy) {
		if(appDownloadStrategy.equals(AppDownloadStrategy.SKIP_DOWNLOAD)) {
			skipDownloadCount++;
			if(skipDownloadCount >= MAX_SKIP_DOWNLOAD_COUNT) {
				logger.info("Approaching the threshold of Google Play anti-robot detection, take a break for {} sec.", MAX_REST_TIME / 1000);
				try {
					Thread.sleep(MAX_REST_TIME);
				} catch (InterruptedException e) {
					logger.info("Continue to work...");
				}
				skipDownloadCount = 0;
			}
		} else {
			skipDownloadCount = 0;
		}
	}
	
	private boolean recursivelyTraceCauseForSpecificExceptionMessage(Throwable throwable, String targetedMsg) {
		boolean found = false;
		Throwable cause = throwable.getCause();
		if(cause != null) {
			if(cause instanceof SQLException) {
				if(cause.getMessage().contains(targetedMsg)) {
					return true;
				}
			} else {
				found = recursivelyTraceCauseForSpecificExceptionMessage(cause, targetedMsg);
			}
		}
		return found;
	}
	
}
