package edu.ntust.idsl.secure.market.util;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.akdeniz.googleplaycrawler.GooglePlayException;

import edu.ntust.idsl.secure.market.exception.UnableToDownloadPaidAppException;
import edu.ntust.idsl.secure.market.util.constant.ResourceConstant;

/**
 * 
 * @author Carl Lu
 *
 */
@ControllerAdvice
public class GlobalExceptionHandler {

	private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);
	
	@ExceptionHandler(NoHandlerFoundException.class)
	public ModelAndView pagNotFound(HttpServletRequest req, Exception exception) throws Exception {
		if (AnnotationUtils.findAnnotation(exception.getClass(), ResponseStatus.class) != null)
			throw exception;
		
		logger.error("Request: " + req.getRequestURI() + " raised " + exception);
		String title = "Page not found.";
		return setupModelAndView(exception, req.getRequestURL().toString(), title, 
				ResourceConstant.ERROR_PAGE_IMG_FOR_HTTP_STATUS_CODE_404, 
				ResourceConstant.ERROR_PAGE_FOR_HTTP_STATUS_CODE_404);
	}
	
	@ExceptionHandler({UnableToDownloadPaidAppException.class, GooglePlayException.class})
	public ModelAndView handleErrorForDownloadPaidApp(HttpServletRequest req, Exception exception) throws Exception {
		if (AnnotationUtils.findAnnotation(exception.getClass(), ResponseStatus.class) != null)
			throw exception;

		logger.error("Request: " + req.getRequestURI() + " raised " + exception);
		String title = "Sorry, something bad happened...";
		if(exception.getClass().equals(GooglePlayException.class)) {
			title = "Sorry, please check the apk id is correct or not...";
		}
		return setupModelAndView(exception, req.getRequestURL().toString(), title, 
				ResourceConstant.ERROR_PAGE_IMG_FOR_INCORRECT_DOWNLOAD_OPERATION, 
				ResourceConstant.ERROR_PAGE_FOR_HTTP_STATUS_CODE_500);
	}
	
	private ModelAndView setupModelAndView(Exception exception, String url, String title, String errImg, String errPage) {
		ModelAndView mav = new ModelAndView();
		mav.addObject("exception", exception);
		mav.addObject("url", String.format("URL: %s", url));
		mav.addObject("title", title);
		mav.addObject("timestamp", String.format("Time: %s", new Date().toString()));
		mav.addObject("msg", exception.getMessage());
		mav.addObject("errImg", errImg);
		mav.setViewName(errPage);
		return mav;
	}
	
}
