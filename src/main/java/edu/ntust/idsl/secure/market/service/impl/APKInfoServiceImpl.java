package edu.ntust.idsl.secure.market.service.impl;

import java.time.Clock;
import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import edu.ntust.idsl.secure.market.domain.APKInfo;
import edu.ntust.idsl.secure.market.domain.AppComparedResult;
import edu.ntust.idsl.secure.market.domain.MaliciousAPKInfo;
import edu.ntust.idsl.secure.market.domain.enums.UploadAppResult;
import edu.ntust.idsl.secure.market.persistence.APKInfoRepository;
import edu.ntust.idsl.secure.market.persistence.MaliciousAPKInfoRepository;
import edu.ntust.idsl.secure.market.service.APKInfoService;
import edu.ntust.idsl.secure.market.util.APKFileHandler;
import edu.ntust.idsl.secure.market.util.AppIntegrityVerifier;
import edu.ntust.idsl.secure.market.util.constant.WebConstant;

/**
 * 
 * @author Carl Lu
 *
 */
@Service
public class APKInfoServiceImpl implements APKInfoService {

	private static final Logger logger = LoggerFactory.getLogger(APKInfoServiceImpl.class);

	@Autowired
	private WebConstant webConstant;
	
	@Autowired
	private APKInfoRepository apkInfoRepository;
	
	@Autowired
	private MaliciousAPKInfoRepository maliciousAPKInfoRepository;
	
	@Autowired
	private APKFileHandler apkFileHandler;
	
	@Autowired
	private AppIntegrityVerifier appIntegrityVerifier;

	@Override
	public APKInfo findById(long id) {
		return apkInfoRepository.findOne(id);
	}

	@Override
	public boolean exists(APKInfo apkInfo) {
		boolean exist = false;
		if(apkInfoRepository.findByAppNameAndVersionCode(apkInfo.getAppName(), apkInfo.getVersionCode()) != null)
			exist = true;
		return exist;
	}

	@Override
	public Page<APKInfo> listAllApps(Pageable pageable) {
		return apkInfoRepository.findAll(pageable);
	}

	@Override
	public AppComparedResult saveAppInfo(String appName, String apkName) throws Exception {
		APKInfo apkInfo = new APKInfo(appName, apkName);
		apkInfo = apkFileHandler.processAPKInfo(apkInfo);
		AppComparedResult result = appIntegrityVerifier.verifyApp(apkInfo);
		persistApkInfo(result);
		apkFileHandler.clearTempResources(apkName);
		return result;
	}
	
	private void persistApkInfo(AppComparedResult appComparedResult) throws RuntimeException {
		if(appComparedResult.getUploadAppResult().equals(UploadAppResult.PASSED_AND_UPLOADABLE)) {
			APKInfo whiteListApp = (APKInfo) appComparedResult.getApkInfo();
			whiteListApp.setVerificationDetail(appComparedResult.getVerificationDetail().getDetailCode());
			whiteListApp.setUploadTime(LocalDateTime.now(Clock.systemDefaultZone()));
			apkInfoRepository.save(whiteListApp);
			logger.info("{} app has been already written into white list.", appComparedResult.getUploadAppResult().getResultDesc());
		} else if(appComparedResult.getUploadAppResult().equals(UploadAppResult.FAILED_AND_UPLOADABLE)) {
			MaliciousAPKInfo blackListApp = apkFileHandler.transformToMaliciousAPK((APKInfo) appComparedResult.getApkInfo());
			blackListApp.setVerificationDetail(appComparedResult.getVerificationDetail().getDetailCode());
			blackListApp.setUploadTime(LocalDateTime.now(Clock.systemDefaultZone()));
			maliciousAPKInfoRepository.save(blackListApp);
			logger.info("{} app has been already written into black list.", appComparedResult.getUploadAppResult().getResultDesc());
		} else {
			logger.info("{} app has no need to write into DB.", appComparedResult.getUploadAppResult().getResultDesc());
		}
	}
	
}
