package edu.ntust.idsl.secure.market.persistence;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import edu.ntust.idsl.secure.market.domain.APKInfo;

/**
 * 
 * @author Carl Lu
 *
 */
@Repository
public interface APKInfoRepository extends JpaRepository<APKInfo, Long> {
	
	List<APKInfo> findByAppName(String appName);
	
	APKInfo findByAppNameAndVersionCode(String appName, String versionCode);
	
	APKInfo findByApkNameAndVersionCodeAndVerificationDetail(String apkName, String versionCode, int verificationDetail);
	
	List<APKInfo> findByApkNameAndVerificationDetailOrderByUploadTimeDesc(String apkName, int verificationDetail);

}
