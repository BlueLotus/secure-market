package edu.ntust.idsl.secure.market.util.crawler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.impl.client.DefaultHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.akdeniz.googleplaycrawler.GooglePlay.AppDetails;
import com.akdeniz.googleplaycrawler.GooglePlay.DetailsResponse;
import com.akdeniz.googleplaycrawler.GooglePlay.Offer;
import com.akdeniz.googleplaycrawler.GooglePlayAPI;
import com.akdeniz.googleplaycrawler.Utils;

import edu.ntust.idsl.secure.market.domain.AppDownloadInfo;
import edu.ntust.idsl.secure.market.exception.UnableToDownloadPaidAppException;
import edu.ntust.idsl.secure.market.util.constant.ResourceConstant;
import edu.ntust.idsl.secure.market.util.constant.WebConstant;

/**
 * 
 * @author Carl Lu
 *
 */
@Component
public class GooglePlayCrawler {
	
	private static Logger logger = LoggerFactory.getLogger(GooglePlayCrawler.class);
	
	@Autowired
	private WebConstant webConstant;
	
	private static GooglePlayAPI service;
	
	public void launch() throws Exception {
		setup();
		checkin();
		login();
		uploadDeviceConfiguration();
	}

	private static HttpClient getProxiedHttpClient(String host, Integer port) throws Exception {
		HttpClient client = new DefaultHttpClient(GooglePlayAPI.getConnectionManager());
		client.getConnectionManager().getSchemeRegistry().register(Utils.getMockedScheme());
		HttpHost proxy = new HttpHost(host, port);
		client.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);
		return client;
	}
	
	private void setup() throws Exception {
		Properties properties = new Properties();
		properties.load(new FileInputStream(webConstant.getPathToGooglePlayCrawlerHome() + "login.conf"));
		String email = properties.getProperty("email");
		String password = properties.getProperty("password");
		String host = properties.getProperty("host");
		String port = properties.getProperty("port");
		service = new GooglePlayAPI(email, password);
		if (host != null && port != null) {
			service.setClient(getProxiedHttpClient(host, Integer.valueOf(port)));
		}
	}
	
	private void checkin() throws Exception {
		service.checkin();
		logger.info("Crawler checkin completed.");
	}
	
	private void login() throws Exception {
		Thread.sleep(5000);
		service.login();
		logger.info("Login completed.");
	}
	
	private void uploadDeviceConfiguration() throws Exception {
		service.uploadDeviceConfig();
	}
	
	public InputStream downloadAPKWithPackageName(final String packageName) throws Exception {
		logger.debug("Preprocessing before download apk: {}.apk", packageName);
		InputStream downloadStream = null;
		DetailsResponse details = service.details(packageName);
		AppDetails appDetails = details.getDocV2().getDetails().getAppDetails();
		Offer offer = details.getDocV2().getOffer(0);
		
		int versionCode = appDetails.getVersionCode();
		int offerType = offer.getOfferType();
		boolean paid = offer.getCheckoutFlowRequired();
		logger.debug("APK info: versionCode = {}, offerType = {}, paid = {}", versionCode, offerType, paid);
		
		if (!paid) {
			downloadStream = service.download(packageName, versionCode, offerType);
		} else {
			throw new UnableToDownloadPaidAppException("Oops, unable to download paid app, please try some other free apps.");
		}
		
		return downloadStream;
	}
	
	public AppDownloadInfo getAppDownloadInfo(final String packageName) throws Exception {
		AppDownloadInfo appDownloadInfo = null;
		DetailsResponse detailsResponse = service.details(packageName);
		AppDetails appDetails = detailsResponse.getDocV2().getDetails().getAppDetails();
		Offer offer = detailsResponse.getDocV2().getOffer(0);
		String appName = detailsResponse.getDocV2().getTitle();
		int versionCode = appDetails.getVersionCode();
		int offerType = offer.getOfferType();
		boolean isPaidApp = offer.getCheckoutFlowRequired();
		if(isPaidApp) {
			logger.debug("This is a paid App, ignore it.");
		} else {
			appDownloadInfo = new AppDownloadInfo();
			appDownloadInfo.setAppName(appName);
			appDownloadInfo.setPackageName(packageName);
			appDownloadInfo.setVersionCode(versionCode);
			appDownloadInfo.setOfferType(offerType);
		}
		return appDownloadInfo;
	}
	
	public void downloadAPKFileWithAppDownloadInfo(final AppDownloadInfo appDownloadInfo) throws Exception {
		InputStream download = null;
		OutputStream output = null;
		String packageName = appDownloadInfo.getPackageName();
		try {
			download = service.download(packageName, appDownloadInfo.getVersionCode(), appDownloadInfo.getOfferType());
			output = new FileOutputStream(new File(webConstant.getPathToUploaded() + File.separator + packageName + ResourceConstant.SUFFIX_FOR_APK_FILE));
			IOUtils.copy(download, output);
		} catch (Exception e) {
			logger.error("Something wrong when donwloading apk file: {}", packageName);
			throw e;
		} finally {
			if(download != null) download.close();
			if(output != null) output.close();
		}
	}
	
}
