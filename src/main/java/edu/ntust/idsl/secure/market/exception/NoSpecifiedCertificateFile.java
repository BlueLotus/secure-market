package edu.ntust.idsl.secure.market.exception;

/**
 * 
 * @author Carl Lu
 *
 */
public class NoSpecifiedCertificateFile extends RuntimeException {

	private static final long serialVersionUID = 6097816184264133973L;
	
	public NoSpecifiedCertificateFile(String msg) {
		super(msg);
	}
	
	public NoSpecifiedCertificateFile(String msg, Throwable throwable) {
		super(msg, throwable);
	}

}
