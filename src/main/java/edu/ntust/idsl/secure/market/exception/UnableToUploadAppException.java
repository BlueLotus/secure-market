package edu.ntust.idsl.secure.market.exception;

/**
 * 
 * @author Carl Lu
 *
 */
public class UnableToUploadAppException extends RuntimeException {

	private static final long serialVersionUID = 3474381410570643058L;
	
	public UnableToUploadAppException(String msg) {
		super(msg);
	}
	
	public UnableToUploadAppException(String msg, Throwable throwable) {
		super(msg, throwable);
	}

}
