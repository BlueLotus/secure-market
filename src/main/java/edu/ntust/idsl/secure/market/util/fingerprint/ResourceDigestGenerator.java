package edu.ntust.idsl.secure.market.util.fingerprint;

import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.ntust.idsl.secure.market.domain.APKResourceDigest;

/**
 * 
 * @author Ssu-Wei Tang, Carl Lu
 *
 */
public class ResourceDigestGenerator {
	
	private static final Logger logger = LoggerFactory.getLogger(ResourceDigestGenerator.class);

	private static final String DIGEST_ALGORITHM = "SHA1";
	private static final String STRING_ENCODING = "UTF-8";
	private static final int BUFFER_SIZE = 4096;
	
	private static MessageDigest messageDigest;
	private static byte[] digestByteArray;
	
	public static APKResourceDigest generateSha1ForFile(InputStream inputData, String fileName) throws Exception {
		APKResourceDigest apkResourceDigest = new APKResourceDigest();
		try {
			messageDigest = MessageDigest.getInstance(DIGEST_ALGORITHM);
			byte[] buffer = new byte[BUFFER_SIZE];
			int contentLength;
			while ((contentLength = inputData.read(buffer)) > 0) {
				messageDigest.update(buffer, 0, contentLength);
			}
			digestByteArray = messageDigest.digest();
			apkResourceDigest.setFileName(fileName);
			apkResourceDigest.setSha1Base64(Base64.encodeBase64String(digestByteArray));
			apkResourceDigest.setSha1Hex(convertDigestsToHex(digestByteArray));
		} catch (NoSuchAlgorithmException e) {
			logger.error("Digest generation algorithm doesn't exist.");
			throw e;
		} catch (IOException e) {
			logger.error("I/O exception occurred when generating resource digest.");
			throw e;
		} finally {
			inputData.close();
		}
		return apkResourceDigest;
	}
	
	public static String generateSha1ForString(String text) throws Exception {
		String hexDigest = null;
		try {
			messageDigest = MessageDigest.getInstance(DIGEST_ALGORITHM);
			messageDigest.reset();
			messageDigest.update(text.getBytes(STRING_ENCODING));
			digestByteArray = messageDigest.digest();
			hexDigest = convertDigestsToHex(digestByteArray);
		} catch (NoSuchAlgorithmException e) {
			logger.error("Digest generation algorithm doesn't exist.");
			throw e;
		} catch (IOException e) {
			logger.error("I/O exception occurred when generating final digest.");
			throw e;
		}
		return hexDigest;
	}

	private static String convertDigestsToHex(byte[] digestByteArray) {
		String hexDigest = "";
		try (Formatter formatter = new Formatter()) {
			for (final byte b : digestByteArray) {
				formatter.format("%02x", b);
			}
			hexDigest = formatter.toString();
		}
		return hexDigest;
	}
	
}
