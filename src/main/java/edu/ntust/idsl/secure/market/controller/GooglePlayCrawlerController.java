package edu.ntust.idsl.secure.market.controller;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import edu.ntust.idsl.secure.market.service.GooglePlayCrawlerService;
import edu.ntust.idsl.secure.market.util.constant.TemplateResource;

/**
 * 
 * @author Carl Lu
 *
 */
@Controller
@RequestMapping(value = "${url.mapping.root}" + "${url.mapping.googleplay.crawler}")
public class GooglePlayCrawlerController {
	
	private static final Logger logger = LoggerFactory.getLogger(GooglePlayCrawlerController.class);
	
	@Autowired
	private TemplateResource template;
	
	@Autowired
	private GooglePlayCrawlerService googlePlayCrawlerService;

	
	@RequestMapping(value = "${url.mapping.googleplay.crawler.crawl}", method = RequestMethod.GET)
	public String provideCrawlerInfo(Model model) {
		model.addAttribute("msg", "Please enter the following information for crawl the APK you need:");
		return template.getWebPageForCrawlAPK();
	}
	
	@RequestMapping(value = "${url.mapping.googleplay.crawler.crawl}", method = RequestMethod.POST)
	public void crawlSpecificAPKFile(@RequestParam("packageName") String packageName, Model model, HttpServletResponse response) throws Exception {
		String apkFileName = packageName + ".apk";
		String headerKey = "Content-Disposition";
        String headerValue = String.format("attachment; filename=\"%s\"", apkFileName);
        
        InputStream inputStream = null;
        OutputStream responseStream = null;
        ByteArrayOutputStream streamForCountSize = new ByteArrayOutputStream();
        try {
        	inputStream = googlePlayCrawlerService.crawlAPKWithPackageName(packageName);
        	if(inputStream != null) {
        		responseStream = response.getOutputStream();
        		response.setHeader(headerKey, headerValue);
        		response.setContentType("application/octet-stream");
        		byte[] buffer = new byte[1024];
        		int bytesRead = -1;
        		
        		while ((bytesRead = inputStream.read(buffer)) != -1) {
        			responseStream.write(buffer, 0, bytesRead);
        			streamForCountSize.write(buffer, 0, bytesRead);
        		}
        		
        		response.setContentLength(streamForCountSize.size());
        	}
		} catch (Exception e) {
			logger.error("Download apk {} failed, the exception message is: {}", packageName, e.getMessage());
		} finally {
			if(inputStream != null) inputStream.close();
			if(responseStream != null) responseStream.close();
		}
	}
	
}
