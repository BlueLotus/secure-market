package edu.ntust.idsl.secure.market.util.constant;

/**
 * 
 * @author Carl Lu
 *
 */
public class ResourceConstant {
	
	public static final String TARGET_KEY = "SHA1-Digest";
	public static final String ANDROID_MANIFEST_XML = "AndroidManifest.xml";
	public static final String RESOURCES_ARSC = "resources.arsc";
	public static final String CLASSES_DEX = "classes.dex";
	
	public static final String DOWNLOAD_FAILED_LIST = "download_failed.txt";
	
	public static final String SUFFIX_FOR_APK_FILE = ".apk";
	public static final String SUFFIX_FOR_PLAIN_TEXT_FILE = ".txt";
	public static final String SUFFIX_FOR_RSA_CERTIFICATE_FILE = ".RSA";
	public static final String SUFFIX_FOR_DSA_CERTIFICATE_FILE = ".DSA";
	public static final String SUFFIX_FOR_CERTIFICATE = ".cer";
	
	public static final String ERROR_MSG_FOR_PKCS_INVALID = "unable to load PKCS7 object";
	public static final String ERROR_MSG_FOR_ITEM_NOT_FOUND = "Item not found.";
	public static final String ERROR_MSG_FOR_DISASSEMBLING_CLASS = "Error occurred while disassembling class";
	public static final String ERROR_MSG_FOR_APK_ITEM_NOT_FOUND = "The item you were attempting to purchase could not be found.";
	public static final String ERROR_MSG_FOR_UTF8MB4_NOT_SUPPORTED = "Incorrect string value:";
	
	public static final String ERROR_PAGE_FOR_HTTP_STATUS_CODE_401 = "/error/401";
	public static final String ERROR_PAGE_FOR_HTTP_STATUS_CODE_404 = "/error/404";
	public static final String ERROR_PAGE_FOR_HTTP_STATUS_CODE_500 = "/error/500";
	
	public static final String ERROR_PAGE_IMG_FOR_HTTP_STATUS_CODE_401 = "/images/not_found.png";
	public static final String ERROR_PAGE_IMG_FOR_HTTP_STATUS_CODE_404 = "/images/not_found.png";
	public static final String ERROR_PAGE_IMG_FOR_HTTP_STATUS_CODE_500 = "/images/angry.png";
	public static final String ERROR_PAGE_IMG_FOR_INCORRECT_DOWNLOAD_OPERATION = "/images/pull_face.png";
	
	public static final int OBTAIN_APK_MANIFEST_ANALYZER_PROCESS = 1;
	public static final int OBTAIN_EXTRACT_DIGITAL_SIGNATURE_PROCESS = 2;

}
