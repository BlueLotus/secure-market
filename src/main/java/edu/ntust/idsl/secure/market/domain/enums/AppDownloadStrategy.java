package edu.ntust.idsl.secure.market.domain.enums;

/**
 * 
 * @author Carl Lu
 *
 */
public enum AppDownloadStrategy {

	SKIP_DOWNLOAD(1, "Skip download"),
	DOWNLOAD_NEW_APP(2, "Download new app"),
	UPDATE_EXISTENT_APP(3, "Update existent app"),;
	
	private int strategyCode;
	private String strategyDesc;
	
	private AppDownloadStrategy(final int strategyCode, final String strategyDesc) {
		this.strategyCode = strategyCode;
		this.strategyDesc = strategyDesc;
	}

	public int getStrategyCode() {
		return strategyCode;
	}

	public String getStrategyDesc() {
		return strategyDesc;
	}
	
}
