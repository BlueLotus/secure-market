package edu.ntust.idsl.secure.market.task;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import edu.ntust.idsl.secure.market.service.GooglePlayCrawlerService;
import edu.ntust.idsl.secure.market.util.FileExplorer;
import edu.ntust.idsl.secure.market.util.constant.ResourceConstant;
import edu.ntust.idsl.secure.market.util.constant.WebConstant;

/**
 * 
 * @author Carl Lu
 *
 */
@Component
public class GooglePlayCrawlerTask {
	
	private static final Logger logger = LoggerFactory.getLogger(GooglePlayCrawlerTask.class);
	
	@Autowired
	private WebConstant webConstant;
	
	@Autowired
	private GooglePlayCrawlerService googlePlayCrawlerService; 
	
	@Scheduled(fixedRate = 86400000)
	public void crawlAppsInBulkAutomatically() {
		try {
			logger.info("Crawl apps in bulk task start.");
			long start = System.currentTimeMillis();
			List<String> categories = FileExplorer.searchFileWithSpecificSuffixInDir(webConstant.getPathToCrawlerList(), ResourceConstant.SUFFIX_FOR_PLAIN_TEXT_FILE);
			googlePlayCrawlerService.crawlAPKForCategories(categories);
			long end = System.currentTimeMillis();
			logger.info("Crawl apps in bulk task has finished, totally cost: {} mins.", (end - start) / 60000);
		} catch (Exception e) {
			logger.error("Something wrong happened when crawling apps from Google Play.");
			logger.error("The error message is: {}", e.getMessage());
		}
	}

}
