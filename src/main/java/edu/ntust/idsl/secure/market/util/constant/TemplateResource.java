package edu.ntust.idsl.secure.market.util.constant;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 
 * @author Carl Lu
 *
 */
@Component
public class TemplateResource {
	
	@Value("${template.root}")
	private String templateRoot;
	
	@Value("${template.dir.market}")
	private String templateDirForMarket;
	
	@Value("${template.dir.app}")
	private String templateDirForApp;
	
	@Value("${template.dir.crawler}")
	private String templateDirForCralwer;
	
	@Value("${template.page.home}")
	private String webPageForHome;
	
	@Value("${template.page.market.list}")
	private String webPageForListApps;
	
	@Value("${template.page.app.upload}")
	private String webPageForUploadAPK;
	
	@Value("${template.page.app.verify.result}")
	private String webPageForVerificationResult;
	
	@Value("${template.page.crawlapk}")
	private String webPageForCrawlAPK;

	public String getTemplateHome() {
		return templateRoot;
	}

	public String getWebPageForHome() {
		return templateRoot + webPageForHome;
	}
	
	public String getWebPageForListApps() {
		return templateRoot + templateDirForMarket + webPageForListApps;
	}

	public String getWebPageForUploadAPK() {
		return templateRoot + templateDirForApp + webPageForUploadAPK;
	}

	public String getWebPageForVerificationResult() {
		return templateRoot + templateDirForApp + webPageForVerificationResult;
	}
	
	public String getWebPageForCrawlAPK() {
		return templateRoot + templateDirForCralwer + webPageForCrawlAPK;
	}
	
}
