package edu.ntust.idsl.secure.market.util.fingerprint;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.TreeMap;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * 
 * @author Ssu-Wei Tang
 *
 */
public class APKFingerPrintHandler {

	public static TreeMap<String, JarEntry> getAPKFileMap(JarFile apkSourceFile) {
		TreeMap<String, JarEntry> apkTreeMap = new TreeMap<String, JarEntry>();
		for (Enumeration<JarEntry> e = apkSourceFile.entries(); e.hasMoreElements();) {
			JarEntry entry = e.nextElement();
			apkTreeMap.put(entry.getName(), entry);
		}
		return apkTreeMap;
	}

	public static void cutFile(InputStream inputData, String outputFolder, String outputfile, String fileSuffix, int fileCutNumber) {
		makeDir(outputFolder);
		try {
			byte[] buffer = new byte[1];
			int splitSize;
			if (inputData.available() % fileCutNumber == 0) {
				splitSize = inputData.available() / fileCutNumber;
			} else {
				splitSize = inputData.available() / fileCutNumber;
				splitSize++;
			}
			for (int i = 0; i < fileCutNumber; i++) {
				int num = 0;
				BufferedOutputStream bos = new BufferedOutputStream(
						new FileOutputStream(outputFolder + File.separator + outputfile + (i + 1) + fileSuffix));
				while (inputData.read(buffer) != -1) {
					bos.write(buffer);
					num++;
					if (num == splitSize) {
						bos.flush();
						bos.close();
						break;
					}
				}
				if (num < splitSize) {
					bos.flush();
					bos.close();
				}
			}
			inputData.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static boolean makeDir(String dir) {
		Boolean result = false;
		File file = new File(dir);
		if (!file.exists()) {
			result = file.mkdir();
		}
		return result;	
	}
	
	public static boolean deleteDirectoryForCutFile(File fileFolder) {
		File[] files = fileFolder.listFiles();
		if (files != null) {
			for (File file : files) {
				if (file.isDirectory()) {
					deleteDirectoryForCutFile(file);
				} else if (file.isFile() && file.exists()) {
					file.delete();
				}
			}
		}
		return fileFolder.delete();
	}
	
}
