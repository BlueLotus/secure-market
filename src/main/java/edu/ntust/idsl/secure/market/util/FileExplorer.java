package edu.ntust.idsl.secure.market.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.ntust.idsl.secure.market.exception.NoSpecifiedCertificateFile;
import edu.ntust.idsl.secure.market.util.constant.ResourceConstant;

/**
 * 
 * @author Carl Lu
 *
 */
public class FileExplorer {
	
	private static final Logger logger = LoggerFactory.getLogger(FileExplorer.class);
	
	public static String findCertificateFileName(JarFile apkFile) throws Exception {
		String entryNameForCertificateFile = null;
		Enumeration<JarEntry> entries = apkFile.entries();
		while (entries.hasMoreElements()) {
			String elementName = entries.nextElement().getName();
			if(elementName.contains(ResourceConstant.SUFFIX_FOR_RSA_CERTIFICATE_FILE)) {
				entryNameForCertificateFile = elementName;
				logger.debug("Found RSA certificate file: {}", entryNameForCertificateFile);
			} else if(elementName.contains(ResourceConstant.SUFFIX_FOR_DSA_CERTIFICATE_FILE)) {
				entryNameForCertificateFile = elementName;
				logger.debug("Found DSA certificate file: {}", entryNameForCertificateFile);
			}
		}
		if(entryNameForCertificateFile == null) {
			logger.error("Could not found any specified certificate file.");
			throw new NoSpecifiedCertificateFile("Could not found any specified certificate file.");
		}
		return entryNameForCertificateFile;
	}
	
	public static void copyFile(File origin, File replica, boolean keepOrigin) throws IOException {
		FileInputStream input = null;
		FileOutputStream output = null;
		try {
			input = new FileInputStream(origin);
			output = new FileOutputStream(replica);
			IOUtils.copy(input, output);
			if(!keepOrigin) {				
				origin.delete();
			}
		} catch (IOException e) {
			logger.error("Error occurered when copying file from {} to {}.", origin.toString(), replica.toString());
			throw e;
		} finally {
			if(input != null) input.close();
			if(output != null) output.close();
		}
	}
	
	public static void deleteFolder(String folder) throws IOException {
		logger.debug("Deleting folder/file {} recursively...", folder);
		Path path = Paths.get(folder);
		Files.walkFileTree(path, new FileVisitor<Path>(){
			@Override
			public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
				Files.delete(dir);
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				Files.delete(file);
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
				logger.error("Something wrong when visit dir/file: {}", exc.getMessage());
				return FileVisitResult.CONTINUE;
			}});
		logger.debug("Delete folder {} successfully.", folder);
	}
	
	public static List<String> searchFileWithSpecificSuffixInDir(String dir, String suffix) throws IOException {
		logger.debug("Searching files with suffix: {} in dir: {}", suffix, dir);
		List<String> matchedList = new ArrayList<String>();
		Path path = Paths.get(dir);
		Files.walkFileTree(path, new FileVisitor<Path>(){
			@Override
			public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				String fileName = file.getFileName().toString();
				if(fileName.contains(suffix)) {
					logger.debug("Found matched file: {}", fileName);
					matchedList.add(fileName);
				}
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
				logger.error("Something wrong when visit dir/file: {}", exc.getMessage());
				return FileVisitResult.CONTINUE;
			}});
		return matchedList;
	}
	
}
