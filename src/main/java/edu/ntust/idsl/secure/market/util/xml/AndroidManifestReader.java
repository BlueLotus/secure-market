package edu.ntust.idsl.secure.market.util.xml;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmlpull.v1.XmlPullParser;

/**
 * 
 * @author Carl Lu
 *
 */
public class AndroidManifestReader extends XMLReader {
	
	private static final Logger logger = LoggerFactory.getLogger(AndroidManifestReader.class);
	
	private static final String ANDROID_MANIFEST_TAG_FOR_MANIFEST = "manifest";
	private static final String ANDROID_MANIFEST_ATTR_FOR_VERSIONCODE = "versionCode";
	private static final String ANDROID_MANIFEST_ATTR_FOR_PACKAGE_NAME = "package";
	private static final String ANDROID_MANIFEST_TAG_FOR_USES_PERMISSION = "uses-permission";
	
	private boolean xmlLoaded = false;
	private String versionCode;
	private String packageName;

	public AndroidManifestReader() {
		super();
	}
	
	public void loadManifest(String androidManifest) {
		try {
			xmlLoaded = setXML(androidManifest);
		} catch (IOException e) {
			logger.error("Load manifest failed, the error message is: {}", e.getMessage());
		}
	}
	
	public String extractVersionCode() {
		String versionCode = null;
		if(xmlLoaded) {
			logger.debug("Extracting versionCode from AndroidManifest.xml...");
			try {
				int type = getEventType();
				while(type != XmlPullParser.END_DOCUMENT) {
					if(type == XmlPullParser.START_TAG)
						if(getName().contains(ANDROID_MANIFEST_TAG_FOR_MANIFEST))
							for(int i = 0; i < getAttributeCount(); i++)
								if(getAttributeName(i).contains(ANDROID_MANIFEST_ATTR_FOR_VERSIONCODE))
									versionCode = getAttributeValueWithAttrIndex(i);
					getNext();
					type = getEventType();
				}
				logger.debug("APK versioncode has extracted successfully.");
			} catch (Exception e) {
				logger.debug("Something wrong when extracting versioncode, the error message is: {}", e.getMessage());
			}
		} else {
			logger.debug("Extract AndroidManifest.xml failed, please check your system environment.");
		}
		return versionCode;
	}
	
	public List<String> extractAllPermissions() {
		List<String> permissions = new ArrayList<String>();
		if(xmlLoaded) {
			logger.debug("Extracting permissions from AndroidManifest.xml...");
			try {
				int type = getEventType();
				while(type != XmlPullParser.END_DOCUMENT) {
					if(type == XmlPullParser.START_TAG)
						if(getName().contains(ANDROID_MANIFEST_TAG_FOR_MANIFEST)) {
							for(int i = 0; i < getAttributeCount(); i++)
								if(getAttributeName(i).contains(ANDROID_MANIFEST_ATTR_FOR_PACKAGE_NAME))
									packageName = getAttributeValueWithAttrIndex(i);
						} else if(getName().contentEquals(ANDROID_MANIFEST_TAG_FOR_USES_PERMISSION)) {
							for(int i = 0; i < getAttributeCount(); i++) 
								permissions.add(getAttributeValueWithAttrIndex(i));
						}
					getNext();
					type = getEventType();
				}
				logger.debug("All permissions are extracted successfully.");
			} catch (IOException e) {
				logger.debug("Something wrong when extracting permissions, the error message is: {}", e.getMessage());
			}
		} else {
			logger.debug("Extract AndroidManifest.xml failed, please check your system environment.");
		}
		return permissions;
	}
	
	public String transformToUsesPermissionString(List<String> usesPermissions) {
		String usesPermissionString = "";
		for (String permission : usesPermissions) {
			usesPermissionString = usesPermissionString.concat(permission).concat(",");
		}
		return usesPermissionString.substring(0, usesPermissionString.length() - 1);
	}
	
	public String getPackageName() {
		return packageName;
	}

	public String getVersionCode() {
		return versionCode;
	}

}
