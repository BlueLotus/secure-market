package edu.ntust.idsl.secure.market.domain;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author Carl Lu
 *
 */
@Entity
@Table(name = "malicious_apk_info")
public class MaliciousAPKInfo implements Serializable, GeneralAPKInfo {

	private static final long serialVersionUID = 2461912158809812825L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "app_name", nullable = false)
	private String appName;
	
	@Column(name = "apk_name", nullable = false)
	private String apkName;
	
	@Column(name = "app_certificate", nullable = false)
	private String appCertificate;
	
	@Column(name = "version_code", nullable = false)
	private String versionCode;
	
	@Column(name = "mnft_entry_size", nullable = false)
	private int mnftEntrySize;
	
	@Column(name = "android_mnft_xml", nullable = false)
	private String androidMnftXml;
	
	@Column(name = "res_arsc", nullable = false)
	private String resourcesArsc;
	
	@Column(name = "cls_dex", nullable = false)
	private String classesDex;
	
	@Column(name = "apk_fingerprint", nullable = false)
	private String apkFingerprint;
	
	@Column(name = "verification_detail", nullable = false)
	private int verificationDetail;
	
	@Column(name = "upload_time", nullable = false)
	private LocalDateTime uploadTime;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getAppName() {
		return appName;
	}
	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getApkName() {
		return apkName;
	}
	public void setApkName(String apkName) {
		this.apkName = apkName;
	}

	public String getAppCertificate() {
		return appCertificate;
	}
	public void setAppCertificate(String appCertificate) {
		this.appCertificate = appCertificate;
	}
	
	public String getVersionCode() {
		return versionCode;
	}
	public void setVersionCode(String versionCode) {
		this.versionCode = versionCode;
	}

	public int getMnftEntrySize() {
		return mnftEntrySize;
	}
	public void setMnftEntrySize(int mnftEntrySize) {
		this.mnftEntrySize = mnftEntrySize;
	}

	public String getAndroidMnftXml() {
		return androidMnftXml;
	}
	public void setAndroidMnftXml(String androidMnftXml) {
		this.androidMnftXml = androidMnftXml;
	}

	public String getResourcesArsc() {
		return resourcesArsc;
	}
	public void setResourcesArsc(String resourcesArsc) {
		this.resourcesArsc = resourcesArsc;
	}

	public String getClassesDex() {
		return classesDex;
	}
	public void setClassesDex(String classesDex) {
		this.classesDex = classesDex;
	}

	public String getApkFingerprint() {
		return apkFingerprint;
	}
	public void setApkFingerprint(String apkFingerprint) {
		this.apkFingerprint = apkFingerprint;
	}
	
	public int getVerificationDetail() {
		return verificationDetail;
	}
	public void setVerificationDetail(int verificationDetail) {
		this.verificationDetail = verificationDetail;
	}
	
	public LocalDateTime getUploadTime() {
		return uploadTime;
	}
	public void setUploadTime(LocalDateTime uploadTime) {
		this.uploadTime = uploadTime;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((androidMnftXml == null) ? 0 : androidMnftXml.hashCode());
		result = prime * result + ((apkFingerprint == null) ? 0 : apkFingerprint.hashCode());
		result = prime * result + ((apkName == null) ? 0 : apkName.hashCode());
		result = prime * result + ((appCertificate == null) ? 0 : appCertificate.hashCode());
		result = prime * result + ((appName == null) ? 0 : appName.hashCode());
		result = prime * result + ((classesDex == null) ? 0 : classesDex.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + mnftEntrySize;
		result = prime * result + ((resourcesArsc == null) ? 0 : resourcesArsc.hashCode());
		result = prime * result + ((uploadTime == null) ? 0 : uploadTime.hashCode());
		result = prime * result + verificationDetail;
		result = prime * result + ((versionCode == null) ? 0 : versionCode.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MaliciousAPKInfo other = (MaliciousAPKInfo) obj;
		if (androidMnftXml == null) {
			if (other.androidMnftXml != null)
				return false;
		} else if (!androidMnftXml.equals(other.androidMnftXml))
			return false;
		if (apkFingerprint == null) {
			if (other.apkFingerprint != null)
				return false;
		} else if (!apkFingerprint.equals(other.apkFingerprint))
			return false;
		if (apkName == null) {
			if (other.apkName != null)
				return false;
		} else if (!apkName.equals(other.apkName))
			return false;
		if (appCertificate == null) {
			if (other.appCertificate != null)
				return false;
		} else if (!appCertificate.equals(other.appCertificate))
			return false;
		if (appName == null) {
			if (other.appName != null)
				return false;
		} else if (!appName.equals(other.appName))
			return false;
		if (classesDex == null) {
			if (other.classesDex != null)
				return false;
		} else if (!classesDex.equals(other.classesDex))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (mnftEntrySize != other.mnftEntrySize)
			return false;
		if (resourcesArsc == null) {
			if (other.resourcesArsc != null)
				return false;
		} else if (!resourcesArsc.equals(other.resourcesArsc))
			return false;
		if (uploadTime == null) {
			if (other.uploadTime != null)
				return false;
		} else if (!uploadTime.equals(other.uploadTime))
			return false;
		if (verificationDetail != other.verificationDetail)
			return false;
		if (versionCode == null) {
			if (other.versionCode != null)
				return false;
		} else if (!versionCode.equals(other.versionCode))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "MaliciousAPKInfo [id=" + id + ", appName=" + appName
				+ ", apkName=" + apkName + ", appCertificate=" + appCertificate
				+ ", versionCode=" + versionCode + ", mnftEntrySize="
				+ mnftEntrySize + ", androidMnftXml=" + androidMnftXml
				+ ", resourcesArsc=" + resourcesArsc + ", classesDex="
				+ classesDex + ", apkFingerprint=" + apkFingerprint
				+ ", verificationDetail=" + verificationDetail
				+ ", uploadTime=" + uploadTime + "]";
	}

}
