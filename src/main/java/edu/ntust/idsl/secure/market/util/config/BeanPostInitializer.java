package edu.ntust.idsl.secure.market.util.config;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.DispatcherServlet;

import edu.ntust.idsl.secure.market.util.FileExplorer;
import edu.ntust.idsl.secure.market.util.constant.WebConstant;

/**
 * 
 * @author Carl Lu
 * 
 */
@Configuration
public class BeanPostInitializer implements BeanPostProcessor {
	
	private static final Logger logger = LoggerFactory.getLogger(BeanPostInitializer.class);
	
	@Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException { 
    	if (bean instanceof DispatcherServlet) {
            ((DispatcherServlet) bean).setThrowExceptionIfNoHandlerFound(true);
        } if(bean instanceof WebConstant) {
        	generateNecessaryDirsAndFilesFromWebConfig((WebConstant) bean);
        }
    	return bean; 
    }

	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		return bean;
	}
	
	public void generateNecessaryDirsAndFilesFromWebConfig(WebConstant constant) {
		collectNecessaryDir(constant).forEach(dir -> detectAndGenerateDir(dir, constant));
	}
	
	private List<File> collectNecessaryDir(WebConstant constant) {
		List<File> directories = new ArrayList<File>();
		directories.add(new File(constant.getPathToSecureMarketHome()));
		directories.add(new File(constant.getPathToGooglePlayCrawlerHome()));
		directories.add(new File(constant.getPathToFailedList()));
		directories.add(new File(constant.getPathToCrawlerList()));
		directories.add(new File(constant.getPathToArchivedList()));
		directories.add(new File(constant.getPathToUploaded()));
		directories.add(new File(constant.getPathToDecompiled()));
		directories.add(new File(constant.getPathToExtracted()));
		directories.add(new File(constant.getPathToSaved()));
		directories.add(new File(constant.getPathToMalicious()));
		return directories;
	}
	
	private void detectAndGenerateDir(File dir, WebConstant constant) {
		if(!dir.exists()) {
			dir.mkdirs();
			if(constant.getPathToGooglePlayCrawlerHome().contains(dir.toString())) {
				logger.debug("No Google Play config exist, generate one.");
				try {
					copyDefaultGooglePlayLoginConfigToDir(dir);
					logger.debug("Google Play login config has already generated successfully.");
				} catch (IOException e) {
					logger.error("Error occurered when copying default Google Play loging config, detail: {}", e.getMessage());
				}
			}
			logger.debug("Dir: {} has generated successfully.", dir.toString());
		} else {
			logger.debug("Dir: {} has already existed.", dir.toString());
		}
	}
	
	private void copyDefaultGooglePlayLoginConfigToDir(File dir) throws IOException {
		File testLoginConfig = new File("./src/main/resources/config/login.conf");
		File defaultLoginConfig = new File(dir.toString() + File.separator + "login.conf");
		try {
			FileExplorer.copyFile(testLoginConfig, defaultLoginConfig, true);
		} catch (Exception e) {
			throw e;
		}
	}
	
}