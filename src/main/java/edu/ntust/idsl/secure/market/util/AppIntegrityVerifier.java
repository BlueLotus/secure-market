package edu.ntust.idsl.secure.market.util;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import edu.ntust.idsl.secure.market.domain.APKInfo;
import edu.ntust.idsl.secure.market.domain.AppComparedResult;
import edu.ntust.idsl.secure.market.domain.MaliciousAPKInfo;
import edu.ntust.idsl.secure.market.domain.enums.MismatchedResource;
import edu.ntust.idsl.secure.market.domain.enums.UploadAppResult;
import edu.ntust.idsl.secure.market.domain.enums.VerificationDetail;
import edu.ntust.idsl.secure.market.persistence.APKInfoRepository;
import edu.ntust.idsl.secure.market.persistence.MaliciousAPKInfoRepository;

/**
 * 
 * @author Carl Lu
 *
 */
@Component
public class AppIntegrityVerifier {
	
	private static final Logger logger = LoggerFactory.getLogger(AppIntegrityVerifier.class);
	
	@Autowired
	private APKInfoRepository apkInfoRepository;
	
	@Autowired
	private MaliciousAPKInfoRepository maliciousAPKInfoRepository;
	
	public AppComparedResult verifyApp(APKInfo unverified) {
		AppComparedResult result = null;
		APKInfo verified = findVerified(unverified);
		MaliciousAPKInfo malicious = maliciousAPKInfoRepository.
				findByApkNameAndVersionCodeAndApkFingerprint(unverified.getApkName(), unverified.getVersionCode(), unverified.getApkFingerprint());
		
		if(verified != null) {
			logger.info("Launching integrity verification...", unverified.getAppName());
			result = verifyIntegrity(verified, unverified);
			if(result.isPassed()) {
				if(result.getVerificationDetail() == null) {
					if(verified.getVerificationDetail() == VerificationDetail.APP_FROM_GOOGLE_PLAY.getDetailCode()) {
						result.setVerificationDetail(VerificationDetail.APP_FROM_GOOGLE_PLAY);					
					} else if(verified.getVerificationDetail() == VerificationDetail.APP_OUTSIDE_GOOGLE_PLAY.getDetailCode()) {
						result.setVerificationDetail(VerificationDetail.APP_OUTSIDE_GOOGLE_PLAY);
					} else if(verified.getVerificationDetail() == VerificationDetail.REPACKAGED_APP_WITH_LANGUAGE_LOCALIZATION.getDetailCode()) {
						result.setVerificationDetail(VerificationDetail.REPACKAGED_APP_WITH_LANGUAGE_LOCALIZATION);
					}
					result.setUploadable(false);
				}
				logger.info("{} has passed the integrity verification test.", unverified.getApkName());
			} else {
				if(malicious != null) {
					if(malicious.getVerificationDetail() == VerificationDetail.REPACKAGED_APP_WITH_MALICIOUS_CODE.getDetailCode()) {
						result.setVerificationDetail(VerificationDetail.REPACKAGED_APP_WITH_MALICIOUS_CODE);
					} else if(malicious.getVerificationDetail() == VerificationDetail.SUSPECTED_MALICIOUS_APP.getDetailCode()) {
						result.setVerificationDetail(VerificationDetail.SUSPECTED_MALICIOUS_APP);
					}
					result.setUploadable(false);
					logger.info("{} has already existed in black list, deny the upload request.", unverified.getApkName());
					showMismatchedResources(result);
				} else {
					result.setVerificationDetail(VerificationDetail.REPACKAGED_APP_WITH_MALICIOUS_CODE);
					result.setUploadable(true);
					result.setApkInfo(unverified);
					logger.info("{} failed to pass the integrity verification test and will be added to black list.", unverified.getApkName());
					showMismatchedResources(result);
				}
			}
		} else {
			if(malicious != null) {
				result = new AppComparedResult();
				if(malicious.getVerificationDetail() == VerificationDetail.REPACKAGED_APP_WITH_MALICIOUS_CODE.getDetailCode()) {
					result.setVerificationDetail(VerificationDetail.REPACKAGED_APP_WITH_MALICIOUS_CODE);
				} else if(malicious.getVerificationDetail() == VerificationDetail.SUSPECTED_MALICIOUS_APP.getDetailCode()) {
					result.setVerificationDetail(VerificationDetail.SUSPECTED_MALICIOUS_APP);
				}
				result.setUploadable(false);
				result.setPassed(false);
				logger.info("{} has already existed in black list, deny the upload request.", unverified.getApkName());
				showMismatchedResources(result);
			} else {
				result = staticAnalysis(verified, unverified);
				if(result.isPassed()) {
					result.setVerificationDetail(VerificationDetail.APP_OUTSIDE_GOOGLE_PLAY);
					result.setUploadable(true);
					result.setApkInfo(unverified);
					logger.info("{} has passed the static analysis.", unverified.getApkName());
				} else {
					result.setVerificationDetail(VerificationDetail.SUSPECTED_MALICIOUS_APP);
					result.setUploadable(true);
					result.setApkInfo(unverified);
					logger.info("{} failed to pass the static analysis and will be added to black list.", unverified.getApkName());
				}
			}
		}
		return determineUploadAppResult(result);
	}
	
	private APKInfo findVerified(APKInfo unverified) {
		APKInfo verified = null;
		if((verified = apkInfoRepository.
				findByApkNameAndVersionCodeAndVerificationDetail(unverified.getApkName(), 
						unverified.getVersionCode(), VerificationDetail.APP_FROM_GOOGLE_PLAY.getDetailCode())) != null) {
			logger.info("Found verified sample for {} from Google Play Store.", unverified.getApkName());
		} else if((verified = apkInfoRepository.
				findByApkNameAndVersionCodeAndVerificationDetail(unverified.getApkName(), 
						unverified.getVersionCode(), VerificationDetail.APP_OUTSIDE_GOOGLE_PLAY.getDetailCode())) != null) {
			logger.info("Found verified sample for {} from Third-party Store.", unverified.getApkName());
		}
		return verified;
	}
	
	private AppComparedResult verifyIntegrity(APKInfo verified, APKInfo unverified) {
		AppComparedResult result = new AppComparedResult();
		List<MismatchedResource> mismatchedResources = new ArrayList<MismatchedResource>();
		if(!unverified.getApkFingerprint().equals(verified.getApkFingerprint())) {
			mismatchedResources.add(MismatchedResource.LIGHTWEIGHT_APK_FINGER_PRINT);
			if(!unverified.getAppCertificate().equals(verified.getAppCertificate())) {
				mismatchedResources.add(MismatchedResource.CERTIFICATE);
			}
			if(!String.valueOf(unverified.getMnftEntrySize()).equals(String.valueOf(verified.getMnftEntrySize()))) {
				mismatchedResources.add(MismatchedResource.MNFT_ENTRY_SIZE);
			}
			if(!unverified.getAndroidMnftXml().equals(verified.getAndroidMnftXml())) {
				mismatchedResources.add(MismatchedResource.ANDROID_MNFT_XML);
			}
			if(!unverified.getClassesDex().equals(verified.getClassesDex())) {
				mismatchedResources.add(MismatchedResource.CLASSES_DEX);
			}
			if(!unverified.getResourcesArsc().equals(verified.getResourcesArsc())) {
				mismatchedResources.add(MismatchedResource.RESOURCES_ARSC);
			}
		}
		result.setMismatchedResources(mismatchedResources);
		if(mismatchedResources.size() > 0) {
			if(isLanguageLocalization(mismatchedResources)) {
				result.setVerificationDetail(VerificationDetail.REPACKAGED_APP_WITH_LANGUAGE_LOCALIZATION);
				result.setPassed(true);
				result.setUploadable(true);
				result.setApkInfo(unverified);
			} else {
				result.setPassed(false);
			}
		} else {
			result.setPassed(true);
		}
		return result;
	}
	
	private boolean isLanguageLocalization(List<MismatchedResource> mismatchResources) {
		boolean result = false;
		if(mismatchResources.size() == 3) {
			if(mismatchResources.contains(MismatchedResource.CERTIFICATE) &&
			   mismatchResources.contains(MismatchedResource.RESOURCES_ARSC) &&
			   mismatchResources.contains(MismatchedResource.LIGHTWEIGHT_APK_FINGER_PRINT)) {
				result = true;
			}
		}
		return result;
	}
	
	/**
	 * Not yet implemented.
	 * 
	 * @param verified
	 * @param unverified
	 * @return
	 */
	private AppComparedResult staticAnalysis(APKInfo verified, APKInfo unverified) {
		AppComparedResult result = new AppComparedResult();
		result.setPassed(true);
		return result;
	}
	
	private AppComparedResult determineUploadAppResult(AppComparedResult comparedResult) {
		if(comparedResult.isPassed() && comparedResult.isUploadable()) {
			comparedResult.setUploadAppResult(UploadAppResult.PASSED_AND_UPLOADABLE);
		} else if(comparedResult.isPassed() && !comparedResult.isUploadable()) {
			comparedResult.setUploadAppResult(UploadAppResult.PASSED_AND_UNUPLOADABLE);
		} else if(!comparedResult.isPassed() && comparedResult.isUploadable()) {
			comparedResult.setUploadAppResult(UploadAppResult.FAILED_AND_UPLOADABLE);
		} else if(!comparedResult.isPassed() && !comparedResult.isUploadable()) {
			comparedResult.setUploadAppResult(UploadAppResult.FAILED_AND_UNUPLOADABLE);
		}
		return comparedResult;
	}
	
	private void showMismatchedResources(AppComparedResult comparedResult) {
		logger.info("This app failed the verification test in the following resources: {}", comparedResult.showMismatchedResources());
	}

}
