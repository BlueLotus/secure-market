package edu.ntust.idsl.secure.market.domain.enums;

/**
 * 
 * @author Carl Lu
 *
 */
public enum VerificationDetail {
	
	APP_FROM_GOOGLE_PLAY(1, "App from Google Play"),
	APP_OUTSIDE_GOOGLE_PLAY(2, "App outside of Google Play"),
	REPACKAGED_APP_WITH_LANGUAGE_LOCALIZATION(3, "App with language localization"),
	REPACKAGED_APP_WITH_MALICIOUS_CODE(4, "App with malicious code"),
	SUSPECTED_MALICIOUS_APP(5, "Suspected malicious app");
	
	private int detailCode;
	private String detailDesc;
	
	VerificationDetail(final int detailCode, final String detailDesc) {
		this.detailCode = detailCode;
		this.detailDesc = detailDesc;
	}

	public int getDetailCode() {
		return detailCode;
	}

	public String getDetailDesc() {
		return detailDesc;
	}
	
}
