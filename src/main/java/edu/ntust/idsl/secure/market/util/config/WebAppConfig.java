package edu.ntust.idsl.secure.market.util.config;

import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.thymeleaf.extras.java8time.dialect.Java8TimeDialect;

/**
 * 
 * @author Carl Lu
 *
 */
@Configuration
public class WebAppConfig extends WebMvcConfigurerAdapter {
	
	/**
	 * This will makes the pagination functionality works well.
	 */
	@Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
    	PageableHandlerMethodArgumentResolver resolver = new PageableHandlerMethodArgumentResolver();
        resolver.setFallbackPageable(new PageRequest(0, 5));
        argumentResolvers.add(resolver);
    }
	
	/**
	 * 
	 * To apply Time API of Java 8 in Thymeleaf template,
	 * 
	 * you must offer Spring a java 8 time dialect object,
	 * 
	 * that's why the Java8TimeDialect be injected here.
	 * 
	 * @return
	 */
	@Bean
	public Java8TimeDialect java8TimeDialect() {
		return new Java8TimeDialect();
	} 
	
}
