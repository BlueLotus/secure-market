package edu.ntust.idsl.secure.market.util.fingerprint;

import java.math.BigInteger;
import java.util.List;

import edu.ntust.idsl.secure.market.domain.APKResourceDigest;


/**
 * 
 * @author Ssu-Wei Tang, Carl Lu
 *
 */
public class XORChainCalculator {
	
	public static String calculateXORSha1Value(List<APKResourceDigest> listForSubFileFingerPrint) {
		BigInteger xorSha1ValueCalculator = null ;
		BigInteger xorSha1InitValue = new BigInteger(listForSubFileFingerPrint.get(0).getSha1Hex(), 16) ;
		if(listForSubFileFingerPrint.size() == 1) {
			xorSha1ValueCalculator=xorSha1InitValue;
		} else {
			for (int j = 1; j < listForSubFileFingerPrint.size(); j++) {
				xorSha1ValueCalculator = new BigInteger(listForSubFileFingerPrint.get(j).getSha1Hex(), 16);
				xorSha1ValueCalculator = xorSha1ValueCalculator.xor(xorSha1InitValue);
				xorSha1InitValue = xorSha1ValueCalculator;
			}
		}
		return String.format("%06x", xorSha1ValueCalculator);		
	}
	
}
