package edu.ntust.idsl.secure.market.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import edu.ntust.idsl.secure.market.domain.APKInfo;
import edu.ntust.idsl.secure.market.domain.AppComparedResult;

/**
 * 
 * @author Carl Lu
 *
 */
public interface APKInfoService {
	
	public APKInfo findById(long id);
	
	public boolean exists(APKInfo apkInfo);
	
	public Page<APKInfo> listAllApps(Pageable pageable);
	
	public AppComparedResult saveAppInfo(String appName, String apkName) throws Exception;

}
