package edu.ntust.idsl.secure.market.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import edu.ntust.idsl.secure.market.domain.AppComparedResult;
import edu.ntust.idsl.secure.market.domain.enums.UploadAppResult;
import edu.ntust.idsl.secure.market.domain.enums.VerificationDetail;
import edu.ntust.idsl.secure.market.exception.UnableToUploadAppException;
import edu.ntust.idsl.secure.market.service.APKInfoService;
import edu.ntust.idsl.secure.market.util.APKFileHandler;
import edu.ntust.idsl.secure.market.util.constant.ResourceConstant;
import edu.ntust.idsl.secure.market.util.constant.WebConstant;
import edu.ntust.idsl.secure.market.util.constant.TemplateResource;

/**
 * 
 * @author Carl Lu
 *
 */
@Controller
@RequestMapping(value = "${url.mapping.root}" + "${url.mapping.app}")
public class UploadAPKController {
	
	private static final Logger logger = LoggerFactory.getLogger(UploadAPKController.class);
	
	@Autowired
	private WebConstant webConstant;
	
	@Autowired
	private TemplateResource template;
	
	@Autowired
	private APKInfoService apkInfoService;
	
	@Autowired
	private APKFileHandler apkFileHandler; 
	
	@RequestMapping(value = "${url.mapping.app.upload}", method = RequestMethod.GET)
	public String uploadApp(Model model) {
		model.addAttribute("msg", "Please enter the following information for uploading your app:");
		return template.getWebPageForUploadAPK();
	}
	
	@Transactional
	@RequestMapping(value = "${url.mapping.app.upload}", method = RequestMethod.POST)
	public String uploadApp(@RequestParam("appName") String appName, @RequestParam("apkFile") MultipartFile apkFile, Model model) throws UnableToUploadAppException {
		String apkName = apkFile.getOriginalFilename();
		if(!apkFile.isEmpty() && !appName.isEmpty() && apkName.contains(ResourceConstant.SUFFIX_FOR_APK_FILE)) {
			try {
				byte[] apkFileDataByte = apkFile.getBytes();
				BufferedOutputStream streamForApkFile = new BufferedOutputStream(new FileOutputStream(new File(webConstant.getPathToUploaded() + apkName)));
				streamForApkFile.write(apkFileDataByte);
				streamForApkFile.close();
				AppComparedResult result = apkInfoService.saveAppInfo(appName, apkName);
				logger.info("App: {} has been processed successfully.", appName);
				model.addAttribute("appName", "App Name: " + appName);
				model.addAttribute("passed", result.isPassed());
				model.addAttribute("hasMismatchedResources", result.getMismatchedResources().size() > 0);
				model.addAttribute("mismatchedResources", result.getMismatchedResources());
				model.addAttribute("msg", obtainVerificationResponseMsg(result));
				if(result.isUploadable()) {
					String destinationDir = chooseDestinationDir(result);
					apkFileHandler.moveUploadedAPKToDir(apkName, destinationDir);
				} else {
					apkFileHandler.removeUploadedAPK(apkName);
				}
			} catch (Exception e) {
				logger.error("App {} could not be uploaded.", appName);
				logger.error("There is something wrong when uploading the apk file, error message: {}", e.getMessage());
				model.addAttribute("msg", String.format("You failed to upload %s, please call the system admin to check the problem.", appName));
			}
		} else {
			model.addAttribute("msg", "You failed to upload the app because the app name/file was empty or incorrect.");
		}
		return template.getWebPageForVerificationResult();
	}
	
	private String chooseDestinationDir(AppComparedResult comparedResult) {
		String destinationDir = null;
		if(comparedResult.getUploadAppResult().equals(UploadAppResult.PASSED_AND_UPLOADABLE)) {
			destinationDir = webConstant.getPathToSaved();
		} else if(comparedResult.getUploadAppResult().equals(UploadAppResult.FAILED_AND_UPLOADABLE)) {
			destinationDir = webConstant.getPathToMalicious();
		}
		return destinationDir;
	}
	
	private String obtainVerificationResponseMsg(AppComparedResult result) {
		String responseMsg = null;
		VerificationDetail detail = result.getVerificationDetail();
		if(detail.equals(VerificationDetail.APP_FROM_GOOGLE_PLAY)) {
			responseMsg = "This app is from Google Play and without any malicious code included.";
		} else if(detail.equals(VerificationDetail.APP_OUTSIDE_GOOGLE_PLAY)) {
			responseMsg = "This app is outside Google Play and without malicious code included.";
		} else if(detail.equals(VerificationDetail.REPACKAGED_APP_WITH_LANGUAGE_LOCALIZATION)) {
			responseMsg = "This is a repackaged app for language localization and without any malicious code included.";
		} else if(detail.equals(VerificationDetail.REPACKAGED_APP_WITH_MALICIOUS_CODE)) {
			responseMsg = "This is a repackaged app which include some malicious code inside, stop uploading to the market.";
		} else if(detail.equals(VerificationDetail.SUSPECTED_MALICIOUS_APP)) {
			responseMsg = "This app is outside Google Play and has some malicious code included.";
		}
		return responseMsg;
	}
	
}
