package edu.ntust.idsl.secure.market.domain;

import java.io.Serializable;
import java.util.List;

import edu.ntust.idsl.secure.market.domain.enums.MismatchedResource;
import edu.ntust.idsl.secure.market.domain.enums.UploadAppResult;
import edu.ntust.idsl.secure.market.domain.enums.VerificationDetail;

/**
 * 
 * @author Carl Lu
 *
 */
public class AppComparedResult implements Serializable {
	
	private static final long serialVersionUID = 7302313147499944136L;
	
	private GeneralAPKInfo apkInfo;
	
	private UploadAppResult uploadAppResult;
	
	private boolean passed;
	
	private boolean uploadable;
	
	private VerificationDetail verificationDetail;
	
	private List<MismatchedResource> mismatchedResources;
	
	public GeneralAPKInfo getApkInfo() {
		return apkInfo;
	}
	public void setApkInfo(GeneralAPKInfo apkInfo) {
		this.apkInfo = apkInfo;
	}
	
	public UploadAppResult getUploadAppResult() {
		return uploadAppResult;
	}
	public void setUploadAppResult(UploadAppResult uploadAppResult) {
		this.uploadAppResult = uploadAppResult;
	}
	
	public boolean isPassed() {
		return passed;
	}
	public void setPassed(boolean passed) {
		this.passed = passed;
	}

	public boolean isUploadable() {
		return uploadable;
	}
	public void setUploadable(boolean uploadable) {
		this.uploadable = uploadable;
	}
	
	public VerificationDetail getVerificationDetail() {
		return verificationDetail;
	}
	public void setVerificationDetail(VerificationDetail verificationDetail) {
		this.verificationDetail = verificationDetail;
	}
	
	public List<MismatchedResource> getMismatchedResources() {
		return mismatchedResources;
	}
	public void setMismatchedResources(List<MismatchedResource> mismatchedResources) {
		this.mismatchedResources = mismatchedResources;
	}
	
	public String showMismatchedResources() {
		String result = ""; 
		for (MismatchedResource mismatchedResource : mismatchedResources) {
			result = result.concat(mismatchedResource.getResourceName()).concat(", ");
		}
		return result.substring(0, result.length() - 2);
	}
}
