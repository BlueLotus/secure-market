package edu.ntust.idsl.secure.market.domain;

import java.io.Serializable;

/**
 * 
 * @author Carl Lu
 *
 */
public class AppDownloadInfo implements Serializable {

	private static final long serialVersionUID = 3946953148808685065L;
	
	private String appName;
	private String packageName;
	private int versionCode;
	private int offerType;
	
	public String getAppName() {
		return appName;
	}
	public void setAppName(String appName) {
		this.appName = appName;
	}
	
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	
	public int getVersionCode() {
		return versionCode;
	}
	public void setVersionCode(int versionCode) {
		this.versionCode = versionCode;
	}
	
	public int getOfferType() {
		return offerType;
	}
	public void setOfferType(int offerType) {
		this.offerType = offerType;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((appName == null) ? 0 : appName.hashCode());
		result = prime * result + offerType;
		result = prime * result + ((packageName == null) ? 0 : packageName.hashCode());
		result = prime * result + versionCode;
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AppDownloadInfo other = (AppDownloadInfo) obj;
		if (appName == null) {
			if (other.appName != null)
				return false;
		} else if (!appName.equals(other.appName))
			return false;
		if (offerType != other.offerType)
			return false;
		if (packageName == null) {
			if (other.packageName != null)
				return false;
		} else if (!packageName.equals(other.packageName))
			return false;
		if (versionCode != other.versionCode)
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "AppDownloadInfo [appName=" + appName + ", packageName="
				+ packageName + ", versionCode=" + versionCode + ", offerType="
				+ offerType + "]";
	}
	
}
