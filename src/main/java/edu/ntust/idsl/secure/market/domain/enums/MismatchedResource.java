package edu.ntust.idsl.secure.market.domain.enums;

/**
 * 
 * @author Carl Lu
 *
 */
public enum MismatchedResource {
	
	VERSIONCODE(1, "Version Code"), 
	MNFT_ENTRY_SIZE(2, "Menifest Entry Size"), 
	ANDROID_MNFT_XML(3, "Android Menifest XML"), 
	RESOURCES_ARSC(4, "Resources Arsc"), 
	CLASSES_DEX(5, "Classes Dex"), 
	CERTIFICATE(6, "Certificate"),
	LIGHTWEIGHT_APK_FINGER_PRINT(7, "Lightweight APK Fingerprint");
	
	private int resourceCode;
	private String resourceName;
	
	MismatchedResource(final int resourceCode, final String resourceName) {
		this.resourceName = resourceName;
		this.resourceCode = resourceCode;	
	}
	
	public int getResourceCode() {
		return resourceCode;
	}
	
	public String getResourceName() {
		return resourceName;
	}
	
}
