package edu.ntust.idsl.secure.market.domain.enums;

/**
 * 
 * @author Carl Lu
 *
 */
public enum UploadAppResult {
	
	PASSED_AND_UPLOADABLE(1, "Passed and uploadable"),
	PASSED_AND_UNUPLOADABLE(2, "Passed and unuploadable"),
	FAILED_AND_UPLOADABLE(3, "Failed and uploadable"),
	FAILED_AND_UNUPLOADABLE(4, "Failed and unuploadable");
	
	private int resultCode;
	private String resultDesc;
	
	private UploadAppResult(final int resultCode, final String resultDesc) {
		this.resultCode = resultCode;
		this.resultDesc = resultDesc;
	}

	public int getResultCode() {
		return resultCode;
	}

	public String getResultDesc() {
		return resultDesc;
	}

}
