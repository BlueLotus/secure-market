package edu.ntust.idsl.secure.market.service;

import java.io.InputStream;
import java.util.List;

/**
 * 
 * @author Carl Lu
 *
 */
public interface GooglePlayCrawlerService {

	public InputStream crawlAPKWithPackageName(String packageName) throws Exception;
	
	public void crawlAPKForCategories(List<String> categories) throws Exception;
	
}
