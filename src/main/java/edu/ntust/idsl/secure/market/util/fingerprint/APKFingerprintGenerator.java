package edu.ntust.idsl.secure.market.util.fingerprint;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.ntust.idsl.secure.market.domain.APKFingerprint;
import edu.ntust.idsl.secure.market.domain.APKResourceDigest;
import edu.ntust.idsl.secure.market.util.FileExplorer;
import edu.ntust.idsl.secure.market.util.constant.WebConstant;
import edu.ntust.idsl.secure.market.util.constant.ResourceConstant;

/**
 * 
 * @author Ssu-Wei Tang, Carl Lu
 *
 */
public class APKFingerprintGenerator {
	
	private static final Logger logger = LoggerFactory.getLogger(APKFingerprintGenerator.class);

	private static final int NUMBER_OF_FILE_SEPERATION = 4;
	
	public static String generateFingerPrintForPrimaryResource(JarFile apkSourceFile, JarEntry entry, String name) throws Exception {
		return XORChainCalculator.calculateXORSha1Value(generateFingerPrintListForSubFileOfPrimaryResource(apkSourceFile, entry, name));
	}

	public static String generateFingerPrintForSubCategoryOfSecondaryResource(List<APKResourceDigest> fingerPrintListForSubCategoryOfSecondaryResource) {
		return XORChainCalculator.calculateXORSha1Value(fingerPrintListForSubCategoryOfSecondaryResource);
	}

	public static String generateFingerPrintForSecondaryResource(List<APKResourceDigest> fingerPrintListForSecondaryResource) {
		return XORChainCalculator.calculateXORSha1Value(fingerPrintListForSecondaryResource);
	}
	
	public static String generateLightweightFingerprintForAPK(String apkName, WebConstant webConstant) throws Exception {
		return ResourceDigestGenerator.generateSha1ForString(XORChainCalculator.calculateXORSha1Value(obtainLightweightResourceDigestListFromAPK(apkName, webConstant)));
	}

	public static String generateFingerPrintForEntireAPKFile(APKFingerprint apkFingerPrint) throws Exception {
		return ResourceDigestGenerator.generateSha1ForString(combineAllFingerprints(apkFingerPrint));
	}
	
	public static List<APKResourceDigest> generateFingerPrintListForSubFileOfPrimaryResource(
			JarFile apkSourceFile, JarEntry jarEntry, String entryName) throws Exception {
		List<APKResourceDigest> fingerPrintListForPrimaryResource = new ArrayList<APKResourceDigest>();

		fingerPrintListForPrimaryResource = new ArrayList<APKResourceDigest>();
		int count = NUMBER_OF_FILE_SEPERATION;
		String fileFolder = apkSourceFile.getName().substring(0, apkSourceFile.getName().indexOf(".apk"));
		String fileName = entryName.substring(0, entryName.indexOf("."));
		String fileSuffix = entryName.substring(entryName.indexOf("."));
		APKFingerPrintHandler.cutFile(apkSourceFile.getInputStream(jarEntry), fileFolder, fileName, fileSuffix, count);

		for (int i = 0; i < count; i++) {
			InputStream is = new BufferedInputStream(new FileInputStream(fileFolder + File.separator + fileName + (i + 1) + fileSuffix));
			fingerPrintListForPrimaryResource.add(ResourceDigestGenerator.generateSha1ForFile(is,  fileName + (i + 1) + fileSuffix));
		}
		return fingerPrintListForPrimaryResource;
	}
	
	private static List<APKResourceDigest> obtainLightweightResourceDigestListFromAPK(String apkName, WebConstant webConstant) throws Exception{
		List<APKResourceDigest> listForResourceDigest = new ArrayList<APKResourceDigest>();
		JarFile apk = new JarFile(new File(webConstant.getPathToUploaded() + apkName));
		try {
			listForResourceDigest.add(generateApkResourceDigestforFile(apk, ResourceConstant.ANDROID_MANIFEST_XML));
			listForResourceDigest.add(generateApkResourceDigestforFile(apk, ResourceConstant.CLASSES_DEX));
			listForResourceDigest.add(generateApkResourceDigestforFile(apk, ResourceConstant.RESOURCES_ARSC));
			listForResourceDigest.add(generateApkResourceDigestforFile(apk, FileExplorer.findCertificateFileName(apk)));
			return listForResourceDigest;
		} catch (Exception e) {
			logger.error("Something wrong when generating lightweight fingerprint.");
			throw e;
		} 
	}
	
	private static APKResourceDigest generateApkResourceDigestforFile(JarFile apk, String fileName) throws Exception {
		InputStream resourceFile = apk.getInputStream(apk.getJarEntry(fileName));
		return ResourceDigestGenerator.generateSha1ForFile(resourceFile, fileName);
	}
	
	private static String combineAllFingerprints(APKFingerprint apkFingerPrint) {
		return apkFingerPrint.getFingerprintForClassesDex().
				concat(apkFingerPrint.getFingerprintForResourcesArsc()).
				concat(apkFingerPrint.getFingerprintForAndroidManifestXml()).
				concat(apkFingerPrint.getFingerprintForRes());
	}
	
}
