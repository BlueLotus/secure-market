package edu.ntust.idsl.secure.market.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Map;
import java.util.jar.Attributes;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import edu.ntust.idsl.secure.market.domain.APKInfo;
import edu.ntust.idsl.secure.market.domain.MaliciousAPKInfo;
import edu.ntust.idsl.secure.market.process.APKManifestReaderProcess;
import edu.ntust.idsl.secure.market.process.ExtractMetaInfProcess;
import edu.ntust.idsl.secure.market.util.constant.WebConstant;
import edu.ntust.idsl.secure.market.util.constant.ResourceConstant;
import edu.ntust.idsl.secure.market.util.fingerprint.APKFingerprintGenerator;
import edu.ntust.idsl.secure.market.util.fingerprint.ResourceDigestGenerator;

/**
 * 
 * @author Carl Lu
 *
 */
@Component
public class APKFileHandler {
	
	private static final Logger logger = LoggerFactory.getLogger(APKFileHandler.class);
	
	@Autowired
	private WebConstant webConstant;
	
	@Autowired
	private ProcessBuilderGenerator processBuilderGenerator;
	
	public APKInfo processAPKInfo(APKInfo apkInfo) throws Exception {
		try {
			apkInfo.setVersionCode(extractVersionCodeForAPK(apkInfo.getApkName()));
			apkInfo = extractMetaInfo(apkInfo);
			apkInfo.setAppCertificate(calculateDigestForAppCertificate(apkInfo.getApkName()));
			apkInfo.setApkFingerprint(APKFingerprintGenerator.generateLightweightFingerprintForAPK(apkInfo.getApkName(), webConstant));
		} catch (Exception e) {
			logger.error("Something wrong happened when processing ApkInfo.");
			throw e;
		}
		return apkInfo;
	}
	
	private String extractVersionCodeForAPK(String apkName) throws Exception {
		APKManifestReaderProcess apkManifestReaderProcess = new APKManifestReaderProcess(webConstant, processBuilderGenerator);
		apkManifestReaderProcess.decompileAPK(apkName);
		apkManifestReaderProcess.extractVersionCode(apkName);
		return apkManifestReaderProcess.getVersionCode();
	}
	
	private APKInfo extractMetaInfo(APKInfo apkInfo) throws IOException {
		JarFile apkSourceFile = null;
		try {
			apkSourceFile = new JarFile(webConstant.getPathToUploaded() + apkInfo.getApkName());
			Manifest manifest = apkSourceFile.getManifest();
			Map<String, Attributes> entryMap = manifest.getEntries();
			apkInfo.setAndroidMnftXml(entryMap.get(ResourceConstant.ANDROID_MANIFEST_XML).getValue(ResourceConstant.TARGET_KEY));
			apkInfo.setClassesDex(entryMap.get(ResourceConstant.CLASSES_DEX).getValue(ResourceConstant.TARGET_KEY));
			apkInfo.setResourcesArsc(entryMap.get(ResourceConstant.RESOURCES_ARSC).getValue(ResourceConstant.TARGET_KEY));
			apkInfo.setMnftEntrySize(entryMap.size());
			logger.debug("Extract APK meta info successfully.");
			return apkInfo;
		} catch (IOException e) {
			logger.error("Somthing wrong when extracting APK meta info.");
			throw e;
		} finally {
			if(apkSourceFile != null) apkSourceFile.close();
		}
	}

	private String calculateDigestForAppCertificate(String apkName) throws Exception {
		String certDigest = null;
		ExtractMetaInfProcess extractMetaInfProcess = new ExtractMetaInfProcess(apkName, webConstant, processBuilderGenerator);
		extractMetaInfProcess.extractCertificateFile();	
		String certificateName = extractMetaInfProcess.getDigitalSignatureFileName();
		String fullyCertificateName = webConstant.getPathToExtracted() + apkName + File.separator + certificateName;
		if(certificateFileExist(fullyCertificateName)) {
			certDigest = ResourceDigestGenerator.generateSha1ForFile(new FileInputStream(fullyCertificateName), certificateName).getSha1Base64();
		}
		return certDigest;
	}
	
	private boolean certificateFileExist(String fullyCertificateName) {
		return new File(fullyCertificateName).exists() ? true : false;
	}
	
	public MaliciousAPKInfo transformToMaliciousAPK(APKInfo verificationFailed) {
		MaliciousAPKInfo malicious = new MaliciousAPKInfo();
		malicious.setAppName(verificationFailed.getAppName());
		malicious.setApkName(verificationFailed.getApkName());
		malicious.setVersionCode(verificationFailed.getVersionCode());
		malicious.setMnftEntrySize(verificationFailed.getMnftEntrySize());
		malicious.setAppCertificate(verificationFailed.getAppCertificate());
		malicious.setAndroidMnftXml(verificationFailed.getAndroidMnftXml());
		malicious.setResourcesArsc(verificationFailed.getResourcesArsc());
		malicious.setClassesDex(verificationFailed.getClassesDex());
		malicious.setApkFingerprint(verificationFailed.getApkFingerprint());
		return malicious;
	}
	
	public void moveUploadedAPKToDir(String apkName, String dirName) throws IOException {
		File uploaded = new File(webConstant.getPathToUploaded() + apkName);
		File saved = new File(dirName + apkName);
		try {
			FileExplorer.copyFile(uploaded, saved, false);
		} catch (Exception e) {
			logger.error("Failed to move uploaded apk file to saved area.");
			throw e;
		}
	}
	
	public void removeUploadedAPK(String apkName) {
		File uploaded = new File(webConstant.getPathToUploaded() + apkName);
		uploaded.delete();
	}
	
	public void recordDownloadFailedAPK(String apkName) {
		Path failedListDir = Paths.get(webConstant.getPathToFailedList(), ResourceConstant.DOWNLOAD_FAILED_LIST);
		String record = apkName + "\n";
		byte recordBytes[] = record.getBytes();
		try (OutputStream outputStream = Files.newOutputStream(
				failedListDir, 
				StandardOpenOption.CREATE, 
				StandardOpenOption.APPEND)) {
			outputStream.write(recordBytes);
		} catch (Exception e) {
			logger.error("Something wrong when recording download failed apk.");
			logger.error("The error message is: {}", e.getMessage());
		}
	}
	
	public void clearTempResources(String apkName) throws IOException {
		FileExplorer.deleteFolder(webConstant.getPathToDecompiled() + apkName);
		FileExplorer.deleteFolder(webConstant.getPathToExtracted() + apkName);
	}

}
