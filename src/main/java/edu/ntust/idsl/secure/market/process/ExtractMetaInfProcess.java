package edu.ntust.idsl.secure.market.process;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.ntust.idsl.secure.market.util.FileExplorer;
import edu.ntust.idsl.secure.market.util.ProcessBuilderGenerator;
import edu.ntust.idsl.secure.market.util.constant.WebConstant;
import edu.ntust.idsl.secure.market.util.constant.ResourceConstant;

/**
 * 
 * @author Carl Lu
 *
 */
public class ExtractMetaInfProcess {
	
	private static final Logger logger = LoggerFactory.getLogger(ExtractMetaInfProcess.class);
	
	private WebConstant webConstant;
	
	private ProcessBuilderGenerator processBuilderGenerator;
	
	private String apkName;
	
	private String digitalSignatureFileName;
	
	private String extractedDirName;
	
	public String getDigitalSignatureFileName() {
		return digitalSignatureFileName;
	}

	public ExtractMetaInfProcess(String apkName, WebConstant webConstant, ProcessBuilderGenerator processBuilderGenerator) {
		this.apkName = apkName;
		this.processBuilderGenerator = processBuilderGenerator;
		this.webConstant = webConstant;
		this.extractedDirName = webConstant.getPathToExtracted() + apkName;
	}
	
	public void extractCertificateFile() throws Exception {
		try {
			createExtractedDir(extractedDirName);
			extractRSAFile();
			extractDigitalSignature();
		} catch (Exception e) {
			logger.error("Something wrong when extracting digital signature file.");
			throw e;
		}
	}
	
	private void createExtractedDir(String dirName) {
		File dir = new File(dirName);
		if(!dir.exists()) {
			dir.mkdir();
			logger.debug("Extracted dir {} created successfully.", dirName);
		}
	}
	
	private void extractRSAFile() throws Exception {
		JarFile apkFile = null;
		String entryNameForCertificateFile = null;
		InputStream entryInputStream = null;
		File certificateFile = null;
		OutputStream rsaOutputStream = null;
		try {
			apkFile = new JarFile(webConstant.getPathToUploaded() + apkName);
			entryNameForCertificateFile = FileExplorer.findCertificateFileName(apkFile);
			digitalSignatureFileName = apkName + getSuffixFromCertFile(entryNameForCertificateFile);
			JarEntry jarEntry = apkFile.getJarEntry(entryNameForCertificateFile);
			entryInputStream = apkFile.getInputStream(jarEntry);
			certificateFile = new File(extractedDirName + File.separator + digitalSignatureFileName);
			rsaOutputStream = new FileOutputStream(certificateFile);
			IOUtils.copy(entryInputStream, rsaOutputStream);
		    logger.debug("Certificate file extracted successfully.");
		} catch (Exception e) {
			throw e;
		} finally {
			if(entryInputStream != null) entryInputStream.close();
		    if(rsaOutputStream != null) rsaOutputStream.close();
			if(apkFile != null) apkFile.close();
		}
	}
	
	private void extractDigitalSignature() throws Exception {
		try {
			processBuilderGenerator.setApkName(apkName);
			processBuilderGenerator.setDigitalSignature(digitalSignatureFileName);
			ProcessBuilder processBuilderForExtractingDigitalSignature = processBuilderGenerator.generateProcessBuilderWithScenario(ResourceConstant.OBTAIN_EXTRACT_DIGITAL_SIGNATURE_PROCESS);
			processBuilderForExtractingDigitalSignature.directory(new File(extractedDirName));
			Process extractDigitalSignatureProcess = processBuilderForExtractingDigitalSignature.start();
			BufferedReader readerForExtractDigitalSignature = new BufferedReader(new InputStreamReader(extractDigitalSignatureProcess.getInputStream()));
			BufferedReader errorReaderForExtractDigitalSignature = new BufferedReader(new InputStreamReader(extractDigitalSignatureProcess.getErrorStream()));
			String extractDigitalSignatureContent;
			while ((extractDigitalSignatureContent = errorReaderForExtractDigitalSignature.readLine()) != null) {
				if(extractDigitalSignatureContent.contains(ResourceConstant.ERROR_MSG_FOR_PKCS_INVALID)) {
					logger.error("Cryptography standard mismatched when extracting digital signature.");
				} else {
					logger.error(extractDigitalSignatureContent);
				}
			}
			while ((extractDigitalSignatureContent = readerForExtractDigitalSignature.readLine()) != null) {
				logger.debug(extractDigitalSignatureContent);
			}
		} catch (Exception e) {
			throw e;
		}
	}
	
	private String getSuffixFromCertFile(String certFile) {
		String suffix = null;
		if(certFile.contains(ResourceConstant.SUFFIX_FOR_RSA_CERTIFICATE_FILE)) {
			suffix = ResourceConstant.SUFFIX_FOR_RSA_CERTIFICATE_FILE;
		} else if(certFile.contains(ResourceConstant.SUFFIX_FOR_DSA_CERTIFICATE_FILE)) {
			suffix = ResourceConstant.SUFFIX_FOR_DSA_CERTIFICATE_FILE;
		}
		return suffix;
	}
	
}
