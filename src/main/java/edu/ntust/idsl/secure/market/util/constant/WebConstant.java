package edu.ntust.idsl.secure.market.util.constant;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class WebConstant {
	
	@Value("${path.apktool}")
	private String pathToAPKTool;
	
	@Value("${path.secure.market.home}")
	private String pathToSecureMarketHome;
	
	@Value("${path.secure.market.googleplaycrawler.home}")
	private String pathToGooglePlayCrawlerHome;
	
	@Value("${path.secure.market.googleplaycrawler.download.list}")
	private String pathToCrawlerList;
	
	@Value("${path.secure.market.googleplaycrawler.failed.list}")
	private String pathToFailedList;
	
	@Value("${path.secure.market.googleplaycrawler.archived.list}")
	private String pathToArchivedList;
	
	@Value("${path.apk.samples.saved}")
	private String pathToSaved;
	
	@Value("${path.apk.samples.malicious}")
	private String pathToMalicious;
	
	@Value("${path.apk.samples.uploaded}")
	private String pathToUploaded;
	
	@Value("${path.apk.samples.extracted}")
	private String pathToExtracted;

	@Value("${path.apk.samples.decompiled}")
	private String pathToDecompiled;
	
	@Value("${path.option.metainfo}")
	private String pathToMetaInfo;
	
	@Value("${command.jar}")
	private String jarCommand;
	
	@Value("${command.openssl}")
	private String opensslCommand;
	
	@Value("${command.apktool}")
	private String apkToolCommand;

	@Value("${command.jar.option.xvf}")
	private String jarOptionForXVF;
	
	@Value("${command.openssl.option.cryptography.standards}")
	private String opensslOptionForCryptoStnd;
	
	@Value("${command.openssl.option.input}")
	private String opensslOptionForInput;
	
	@Value("${command.openssl.option.printcerts}")
	private String opensslOptionForPrintCerts;
	
	@Value("${command.openssl.option.inform}")
	private String opensslOptionForInform;
	
	@Value("${command.openssl.option.inform.format}")
	private String opensslOptionForInformFormat;
	
	@Value("${command.openssl.option.output}")
	private String opensslOptionForOutput;
	
	@Value("${command.apktool.option.decompile}")
	private String apkToolOptionForDecompile;

	@Value("${command.apktool.option.overwrite}")
	private String apkToolOptionForOverwrite;
	
	@Value("${command.apktool.option.match.original}")
	private String apkToolOptionForMatchOriginal;

	@Value("${command.apktool.option.output}")
	private String apkToolOptionForOutput;

	public String getPathToAPKTool() {
		return pathToAPKTool;
	}
	
	public String getPathToSecureMarketHome() {
		return pathToSecureMarketHome;
	}

	public String getPathToGooglePlayCrawlerHome() {
		return pathToGooglePlayCrawlerHome;
	}
	
	public String getPathToCrawlerList() {
		return pathToCrawlerList;
	}

	public String getPathToFailedList() {
		return pathToFailedList;
	}

	public String getPathToArchivedList() {
		return pathToArchivedList;
	}

	public String getPathToSaved() {
		return pathToSaved;
	}

	public String getPathToMalicious() {
		return pathToMalicious;
	}

	public String getPathToUploaded() {
		return pathToUploaded;
	}

	public String getPathToExtracted() {
		return pathToExtracted;
	}

	public String getPathToDecompiled() {
		return pathToDecompiled;
	}
	
	public String getPathToMetaInfo() {
		return pathToMetaInfo;
	}

	public String getJarCommand() {
		return jarCommand;
	}

	public String getOpensslCommand() {
		return opensslCommand;
	}

	public String getApkToolCommand() {
		return apkToolCommand;
	}

	public String getJarOptionForXVF() {
		return jarOptionForXVF;
	}

	public String getOpensslOptionForCryptoStnd() {
		return opensslOptionForCryptoStnd;
	}

	public String getOpensslOptionForInput() {
		return opensslOptionForInput;
	}

	public String getOpensslOptionForPrintCerts() {
		return opensslOptionForPrintCerts;
	}

	public String getOpensslOptionForInform() {
		return opensslOptionForInform;
	}

	public String getOpensslOptionForInformFormat() {
		return opensslOptionForInformFormat;
	}

	public String getOpensslOptionForOutput() {
		return opensslOptionForOutput;
	}

	public String getApkToolOptionForDecompile() {
		return apkToolOptionForDecompile;
	}

	public String getApkToolOptionForOverwrite() {
		return apkToolOptionForOverwrite;
	}

	public String getApkToolOptionForMatchOriginal() {
		return apkToolOptionForMatchOriginal;
	}

	public String getApkToolOptionForOutput() {
		return apkToolOptionForOutput;
	}

}
