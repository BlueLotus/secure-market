CREATE SCHEMA secured_market_db ;


CREATE TABLE apk_info (
  id int(10) NOT NULL AUTO_INCREMENT,
  app_name varchar(100) NOT NULL,
  apk_name varchar(150) NOT NULL,
  version_code varchar(20) NOT NULL,
  mnft_entry_size int(5) NOT NULL,
  app_certificate varchar(50) NOT NULL,
  android_mnft_xml varchar(30) NOT NULL,
  res_arsc varchar(30) NOT NULL,
  cls_dex varchar(30) NOT NULL,
  apk_fingerprint varchar(50) NOT NULL,
  verification_detail int(2) NOT NULL DEFAULT '1',
  upload_time datetime NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table for apk file informations.';


CREATE TABLE malicious_apk_info (
  id int(10) NOT NULL AUTO_INCREMENT,
  app_name varchar(100) NOT NULL,
  apk_name varchar(150) NOT NULL,
  version_code varchar(20) NOT NULL,
  mnft_entry_size int(5) NOT NULL,
  app_certificate varchar(50) NOT NULL,
  android_mnft_xml varchar(30) NOT NULL,
  res_arsc varchar(30) NOT NULL,
  cls_dex varchar(30) NOT NULL,
  apk_fingerprint varchar(50) NOT NULL,
  verification_detail int(2) NOT NULL DEFAULT '4',
  upload_time datetime NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table for malicious apk file informations.';
